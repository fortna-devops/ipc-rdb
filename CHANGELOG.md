# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-06-23

### Add
- Port all SQLAlchemy model changes from Aug20 schema (`master` branch of `mhs-rdb` repo).
- Generate Alembic schema migration script for move to Aug20 schema.

#TODO :  Port Conftest.py from mhs-rdb repository
import pytest
from alchemy_mock.mocking import UnifiedAlchemyMagicMock


@pytest.fixture(scope='function')
def session_execute(mocker):
    return mocker.patch('ipc_rdb.session.Session.execute')


@pytest.fixture(scope='function')
def db_session():
    return UnifiedAlchemyMagicMock()

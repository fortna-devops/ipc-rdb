"""Add Analytics and remove Threshold_types

Revision ID: d7ba0b29a392
Revises: 8f81127e766b
Create Date: 2021-09-28 16:54:40.652613

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'd7ba0b29a392'
down_revision = '8f81127e766b'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('analytics',
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('active', sa.Boolean(), nullable=True),
    sa.Column('amount_data_min', sa.Integer(), nullable=True),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('name'),
    schema='equipments'
    )
    op.execute("INSERT INTO analytics  VALUES ('default_threshold_analytic', True, 10, now())")
    op.execute("INSERT INTO analytics  VALUES ('linear_regression', False, 10, now())")

    op.add_column('thresholds', sa.Column('analytic', sa.String(), nullable=True), schema='equipments')
    op.execute("UPDATE thresholds SET analytic = 'default_threshold_analytic' where type_name = 'iso' ")
    op.execute("UPDATE thresholds SET analytic = 'linear_regression' where type_name='linear_regression' ")
    op.alter_column('thresholds', 'analytic', existing_type=sa.String(), nullable=False, schema='equipments')

    op.add_column('thresholds', sa.Column('violations', sa.Integer(), nullable=True), schema='equipments')
    op.execute("UPDATE thresholds SET violations = 5 where type_name in ('iso', 'default_threshold_analytic', 'linear_regression') ")
    op.alter_column('thresholds', 'violations', existing_type=sa.Integer(), nullable=False, schema='equipments')

    op.add_column('thresholds', sa.Column('samples', sa.Integer(), nullable=True), schema='equipments')
    op.add_column('thresholds', sa.Column('duration_min', sa.Integer(), nullable=True), schema='equipments')
    
    op.drop_constraint('thresholds_type_name_fkey', 'thresholds', schema='equipments', type_='foreignkey')
    op.create_foreign_key('thresholds_analytic_fkey', 'thresholds', 'analytics', ['analytic'], ['name'], source_schema='equipments', referent_schema='equipments', ondelete='RESTRICT')
    
    op.drop_column('thresholds', 'type_name', schema='equipments')
    op.drop_table('threshold_types', schema='equipments')


def downgrade():
    op.create_table('threshold_types',
    sa.Column('name', sa.String(), autoincrement=False, nullable=False),
    sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('name', name='threshold_types_pkey'),
    schema='equipments'
    )
    op.execute("INSERT INTO threshold_types  VALUES ('iso', now())")
    op.execute("INSERT INTO threshold_types  VALUES ('linear_regression', now())")

    op.add_column('thresholds', sa.Column('type_name', sa.String(), autoincrement=False, nullable=True), schema='equipments')
    op.execute("UPDATE thresholds SET type_name = 'iso' where analytic='default_threshold_analytic' ")
    op.execute("UPDATE thresholds SET type_name = 'linear_regression' where analytic='linear_regression' ")
    op.alter_column('thresholds', 'type_name', existing_type=sa.String(), nullable=False, schema='equipments')

    op.drop_constraint('thresholds_analytic_fkey', 'thresholds', schema='equipments', type_='foreignkey')
    op.create_foreign_key('thresholds_type_name_fkey', 'thresholds', 'threshold_types', ['type_name'], ['name'], source_schema='equipments', referent_schema='equipments', ondelete='RESTRICT')

    op.drop_column('thresholds', 'duration_min', schema='equipments')
    op.drop_column('thresholds', 'samples', schema='equipments')
    op.drop_column('thresholds', 'violations', schema='equipments')
    op.drop_column('thresholds', 'analytic', schema='equipments')

    op.drop_table('analytics', schema='equipments')

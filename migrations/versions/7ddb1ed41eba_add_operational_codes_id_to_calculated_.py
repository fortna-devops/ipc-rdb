"""Add operation_codes_id to calculated tables

Revision ID: 7ddb1ed41eba
Revises: 4f3c6ba2b74e
Create Date: 2021-10-14 11:07:30.673089

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7ddb1ed41eba'
down_revision = '4f3c6ba2b74e'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('calculated_float_data', sa.Column('operational_code_id', sa.BigInteger(), nullable=False), schema='equipments')
    op.create_foreign_key('calculated_float_data_operational_code_id_fkey', 'calculated_float_data', 'operational_codes', ['operational_code_id'], ['id'], source_schema='equipments', referent_schema='equipments', onupdate='CASCADE', ondelete='RESTRICT')

    op.add_column('calculated_int_data', sa.Column('operational_code_id', sa.BigInteger(), nullable=False), schema='equipments')
    op.create_foreign_key('calculated_int_data_operational_code_id_fkey', 'calculated_int_data', 'operational_codes', ['operational_code_id'], ['id'], source_schema='equipments', referent_schema='equipments', onupdate='CASCADE', ondelete='RESTRICT')

    op.add_column('calculated_string_data', sa.Column('operational_code_id', sa.BigInteger(), nullable=False), schema='equipments')
    op.create_foreign_key('calculated_string_data_operational_code_id_fkey', 'calculated_string_data', 'operational_codes', ['operational_code_id'], ['id'], source_schema='equipments', referent_schema='equipments', onupdate='CASCADE', ondelete='RESTRICT')

def downgrade():
    op.drop_constraint('calculated_string_data_operational_code_id_fkey', 'calculated_string_data', schema='equipments', type_='foreignkey')
    op.drop_column('calculated_string_data', 'operational_code_id', schema='equipments')

    op.drop_constraint('calculated_int_data_operational_code_id_fkey', 'calculated_int_data', schema='equipments', type_='foreignkey')
    op.drop_column('calculated_int_data', 'operational_code_id', schema='equipments')

    op.drop_constraint('calculated_float_data_operational_code_id_fkey', 'calculated_float_data', schema='equipments', type_='foreignkey')
    op.drop_column('calculated_float_data', 'operational_code_id', schema='equipments')

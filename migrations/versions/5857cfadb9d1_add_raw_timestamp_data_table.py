"""Add raw_timestamp_data table

Revision ID: 5857cfadb9d1
Revises: 3a140e0d32c8
Create Date: 2021-11-30 13:42:38.238085

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5857cfadb9d1'
down_revision = '3a140e0d32c8'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('calculated_timestamp_data',
    sa.Column('id', sa.BigInteger(), nullable=False),
    sa.Column('metric_id', sa.Integer(), nullable=False),
    sa.Column('value', sa.DateTime(), nullable=False),
    sa.Column('operational_code_id', sa.BigInteger(), nullable=False),
    sa.Column('timestamp', sa.DateTime(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),


    sa.ForeignKeyConstraint(['metric_id'], ['equipments.metrics.id'], ondelete='RESTRICT'),
    sa.ForeignKeyConstraint(['operational_code_id'], ['equipments.operational_codes.id'], onupdate='CASCADE', ondelete='RESTRICT'),
    sa.PrimaryKeyConstraint('id'),
    schema='equipments'
    )

    #NOTE: Abbreviate index names here because SA rejects names longer than 63 chars but 
    #postgres will override the names below to the usual convention
    op.create_index('calc_timestamp_data_metric_id_timestamp_minute_flags_index', 'calculated_timestamp_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('calc_timestamp_data_metric_id_timestamp_hour_flags_index', 'calculated_timestamp_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    op.execute('alter table calculated_timestamp_data OWNER to mhs')


    op.create_table('raw_timestamp_data',
    sa.Column('id', sa.BigInteger(), nullable=False),
    sa.Column('metric_id', sa.Integer(), nullable=False),
    sa.Column('value', sa.DateTime(), nullable=False),
    sa.Column('operational_code_id', sa.BigInteger(), nullable=False),
    sa.Column('timestamp', sa.DateTime(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['metric_id'], ['equipments.metrics.id'], ondelete='RESTRICT'),
    sa.ForeignKeyConstraint(['operational_code_id'], ['equipments.operational_codes.id'], onupdate='CASCADE', ondelete='RESTRICT'),
    sa.PrimaryKeyConstraint('id'),
    schema='equipments'
    )
    op.create_index('raw_timestamp_data_metric_id_timestamp_minute_flags_index', 'raw_timestamp_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('raw_timestamp_data_metric_id_timestamp_hour_flags_index', 'raw_timestamp_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    op.execute('alter table raw_timestamp_data OWNER to mhs')


def downgrade():
    op.drop_table('raw_timestamp_data', schema='equipments')
    op.drop_table('calculated_timestamp_data', schema='equipments')

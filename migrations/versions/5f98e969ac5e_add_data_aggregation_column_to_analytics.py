"""Add data_aggregation column to Analytics

Revision ID: 5f98e969ac5e
Revises: d7ba0b29a392
Create Date: 2021-09-30 09:50:19.370047

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5f98e969ac5e'
down_revision = 'd7ba0b29a392'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("CREATE TYPE dataaggregationenum AS ENUM ('NO_AGGREGATION', 'AVG_PER_MICROSECOND', 'AVG_PER_MILLISECOND', 'AVG_PER_SECOND', 'AVG_PER_MINUTE', 'AVG_PER_HOUR', 'AVG_PER_DAY', 'AVG_PER_WEEK')")

    # op.drop_constraint('alarms_type_name_color_fkey', 'alarms', schema='equipments', type_='foreignkey')
    # op.create_foreign_key(None, 'alarms', 'alarm_configs', ['type_name', 'color'], ['type_name', 'color'], source_schema='equipments', referent_schema='equipments', ondelete='RESTRICT')
    op.add_column('analytics', sa.Column('data_aggregation', sa.Enum('NO_AGGREGATION', 'AVG_PER_MICROSECOND', 'AVG_PER_MILLISECOND', 'AVG_PER_SECOND', 'AVG_PER_MINUTE', 'AVG_PER_HOUR', 'AVG_PER_DAY', 'AVG_PER_WEEK', name='dataaggregationenum'), nullable=True), schema='equipments')
    op.execute("UPDATE analytics SET data_aggregation = 'AVG_PER_SECOND' where name = 'default_threshold_analytic' ")
    op.execute("UPDATE analytics SET data_aggregation = 'NO_AGGREGATION' where name='linear_regression' ")
    op.alter_column('thresholds', 'analytic', existing_type=sa.Enum('NO_AGGREGATION', 'AVG_PER_MICROSECOND', 'AVG_PER_MILLISECOND', 'AVG_PER_SECOND', 'AVG_PER_MINUTE', 'AVG_PER_HOUR', 'AVG_PER_DAY', 'AVG_PER_WEEK', name='dataaggregationenum'), nullable=False, schema='equipments')


def downgrade():
    
    op.drop_column('analytics', 'data_aggregation', schema='equipments')
    op.execute("DROP TYPE dataaggregationenum")

    # op.drop_constraint(None, 'alarms', schema='equipments', type_='foreignkey')
    # op.create_foreign_key('alarms_type_name_color_fkey', 'alarms', 'alarm_configs', ['type_name', 'color'], ['type_name', 'color'], source_schema='equipments', referent_schema='equipments')
    # ### end Alembic commands ###

"""foreign key correction

Revision ID: a11b5c0eb8dd
Revises: f85651b46c38
Create Date: 2021-07-16 10:49:16.971962

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'a11b5c0eb8dd'
down_revision = 'f85651b46c38'
branch_labels = None
depends_on = None


def upgrade():
    #op.drop_table('alembic_version')
    op.drop_constraint('operational_groups_type_id_fkey', 'operational_groups', schema='equipments', type_='foreignkey')
    op.create_foreign_key('operational_groups_operational_types_id_fkey', 'operational_groups', 'operational_types', ['type_id'], ['id'], source_schema='equipments', referent_schema='equipments', ondelete='RESTRICT')
    op.alter_column('threshold_types', 'updated_at',
               existing_type=postgresql.TIMESTAMP(),
               nullable=False,
               schema='equipments')
    op.add_column('thresholds', sa.Column('operational_type_id', sa.Integer(), nullable=False), schema='equipments')
    op.alter_column('thresholds', 'updated_at',
               existing_type=postgresql.TIMESTAMP(),
               nullable=False,
               schema='equipments')
    op.drop_constraint('thresholds_operational_types_id_fkey', 'thresholds', schema='equipments', type_='foreignkey')
    op.create_foreign_key('thresholds_operational_types_id_fkey', 'thresholds', 'operational_types', ['operational_type_id'], ['id'], source_schema='equipments', referent_schema='equipments', ondelete='RESTRICT')
    op.drop_column('thresholds', 'operational_code_id', schema='equipments')


def downgrade():
    op.add_column('thresholds', sa.Column('operational_code_id', sa.INTEGER(), autoincrement=False, nullable=False), schema='equipments')
    op.drop_constraint('thresholds_operational_types_id_fkey', 'thresholds', schema='equipments', type_='foreignkey')
    op.create_foreign_key('thresholds_operational_types_id_fkey', 'thresholds', 'operational_types', ['operational_code_id'], ['id'], source_schema='equipments', referent_schema='equipments', ondelete='RESTRICT')
    op.alter_column('thresholds', 'updated_at',
               existing_type=postgresql.TIMESTAMP(),
               nullable=True,
               schema='equipments')
    op.drop_column('thresholds', 'operational_type_id', schema='equipments')
    op.alter_column('threshold_types', 'updated_at',
               existing_type=postgresql.TIMESTAMP(),
               nullable=True,
               schema='equipments')
    op.drop_constraint('operational_groups_operational_types_id_fkey', 'operational_groups', schema='equipments', type_='foreignkey')
    op.create_foreign_key('operational_groups_type_id_fkey', 'operational_groups', 'operational_groups', ['type_id'], ['id'], source_schema='equipments', referent_schema='equipments', ondelete='RESTRICT')
    #op.create_table('alembic_version',
    #sa.Column('version_num', sa.VARCHAR(length=32), autoincrement=False, nullable=False),
    #sa.PrimaryKeyConstraint('version_num', name='alembic_version_pkc')
    #)

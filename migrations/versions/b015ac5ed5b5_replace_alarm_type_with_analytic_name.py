"""Replace alarm_type with analytic_name

Revision ID: b015ac5ed5b5
Revises: 98979ea58827
Create Date: 2021-10-13 12:20:02.120576

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'b015ac5ed5b5'
down_revision = '98979ea58827'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('alarm_configs', sa.Column('analytic_name', sa.String(), primary_key=True, nullable=True), schema='equipments')
    op.drop_constraint('alarm_configs_type_name_fkey', 'alarm_configs', schema='equipments', type_='foreignkey')
    op.create_foreign_key('alarm_configs_analytic_name_fkey', 'alarm_configs', 'analytics', 
                            ['analytic_name'], ['name'], source_schema='equipments', 
                            referent_schema='equipments', onupdate = 'CASCADE', ondelete='RESTRICT')

    op.execute("UPDATE alarm_configs SET analytic_name = type_name")
    op.alter_column('alarm_configs', 'analytic_name', existing_type=sa.String(), nullable=False, schema='equipments')

    op.add_column('alarms', sa.Column('analytic_name', sa.String(), primary_key=True, nullable=True), schema='equipments')
    op.create_foreign_key('alarms_analytic_name_fkey', 'alarms', 'analytics', ['analytic_name'], 
                        ['name'], source_schema='equipments', referent_schema='equipments', 
                        onupdate = 'CASCADE', ondelete='RESTRICT')

    op.drop_constraint('alarms_type_name_fkey', 'alarms', schema='equipments', type_='foreignkey')
    op.drop_constraint('alarms_type_name_color_fkey', 'alarms', schema='equipments', type_='foreignkey')

    op.execute("UPDATE alarms SET analytic_name = type_name")
    op.alter_column('alarms', 'analytic_name', existing_type=sa.String(), nullable=False, schema='equipments')
    
    # Dropping column automatically drop indexes built on the column
    op.drop_column('alarms', 'type_name', schema='equipments')
    op.create_index ('idx_started_at_metric_id_analytic_name', 'alarms', ['started_at', 'metric_id', 'analytic_name' ], postgresql_using='btree')
    # op.create_index ('idx_analytic_name_metric_id_tsrange', 'alarms', 
    #                 ['analytic_name', 'metric_id', sa.text("tsrange(started_at, ended_at, '[]')")],
    #                  postgresql_using='gist')

    op.drop_column('alarm_configs', 'type_name', schema='equipments')
    op.create_primary_key('alarm_configs_pkey','alarm_configs',['analytic_name','color'], schema='equipments')
    op.create_foreign_key('alarms_analytic_name_color_fkey', 'alarms', 'alarm_configs', ['analytic_name', 'color'],
                            ['analytic_name', 'color'], source_schema='equipments', referent_schema='equipments',
                            onupdate = 'CASCADE', ondelete='RESTRICT')

    op.drop_table('alarm_types', schema='equipments')

def downgrade():
    op.create_table('alarm_types',
    sa.Column('name', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('created_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.PrimaryKeyConstraint('name', name='alarm_types_pkey'),
    schema='equipments'
    )

    op.add_column('alarm_configs', sa.Column('type_name', sa.VARCHAR(), autoincrement=False, nullable=True), schema='equipments')
    op.drop_constraint('alarm_configs_analytic_name_fkey', 'alarm_configs', schema='equipments', type_='foreignkey')
    op.create_foreign_key('alarm_configs_type_name_fkey', 'alarm_configs', 'alarm_types', ['type_name'], ['name'],
                            source_schema='equipments', referent_schema='equipments', ondelete='RESTRICT')

    op.execute("UPDATE alarm_configs SET type_name = analytic_name")
    op.alter_column('alarm_configs', 'type_name', existing_type=sa.String(), nullable=False, schema='equipments')

    op.add_column('alarms', sa.Column('type_name', sa.String(), autoincrement=False, nullable=True), schema='equipments')
    op.drop_constraint('alarms_analytic_name_color_fkey', 'alarms', schema='equipments', type_='foreignkey')
    op.create_foreign_key('alarms_type_name_fkey', 'alarms', 'alarm_types', ['type_name'], ['name'],
                            source_schema='equipments', referent_schema='equipments', ondelete='RESTRICT')

    op.create_index ('idx_started_at_metric_id_type_name', 'alarms',
                    ['started_at', 'metric_id', 'type_name'], postgresql_using='btree')


    op.execute("UPDATE alarms SET type_name = analytic_name")
    op.alter_column('alarms', 'type_name', existing_type=sa.String(), nullable=False, schema='equipments')

    op.drop_column('alarms', 'analytic_name', schema='equipments')

    op.drop_column('alarm_configs', 'analytic_name', schema='equipments')
    op.create_primary_key('alarm_configs_pkey','alarm_configs',['type_name','color'], schema='equipments')
    op.create_foreign_key('alarms_type_name_color_fkey', 'alarms', 'alarm_configs', ['type_name', 'color'],
                            ['type_name', 'color'], source_schema='equipments', referent_schema='equipments')


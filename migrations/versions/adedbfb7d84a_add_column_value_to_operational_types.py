"""Add column value to operational_types

Revision ID: adedbfb7d84a
Revises: 7ddb1ed41eba
Create Date: 2021-10-15 15:01:39.119421

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'adedbfb7d84a'
down_revision = '7ddb1ed41eba'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('operational_types', sa.Column('value', sa.BigInteger(), nullable=True), schema='equipments')
    op.execute("UPDATE operational_types SET value=0")
    op.alter_column('operational_types', 'value', existing_type=sa.BigInteger(), nullable=False, schema='equipments')

def downgrade():
    op.drop_column('operational_types', 'value', schema='equipments')

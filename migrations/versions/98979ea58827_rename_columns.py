"""Rename columns

Revision ID: 98979ea58827
Revises: 5f98e969ac5e
Create Date: 2021-10-08 20:19:10.868778

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '98979ea58827'
down_revision = '5f98e969ac5e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    # op.drop_constraint('alarms_type_name_color_fkey', 'alarms', schema='equipments', type_='foreignkey')
    # op.create_foreign_key(None, 'alarms', 'alarm_configs', ['type_name', 'color'], ['type_name', 'color'], source_schema='equipments', referent_schema='equipments', ondelete='RESTRICT')
    op.add_column('analytics', sa.Column('amount_data', sa.Integer(), nullable=True), schema='equipments')
    op.execute("UPDATE analytics SET amount_data = amount_data_min")
    op.alter_column('analytics', 'amount_data', existing_type=sa.Integer(), nullable=False, schema='equipments')


    op.alter_column('analytics', 'active',
               existing_type=sa.BOOLEAN(),
               nullable=False,
               schema='equipments')
    op.alter_column('analytics', 'data_aggregation',
               existing_type=postgresql.ENUM('NO_AGGREGATION', 'AVG_PER_MICROSECOND', 'AVG_PER_MILLISECOND', 'AVG_PER_SECOND', 'AVG_PER_MINUTE', 'AVG_PER_HOUR', 'AVG_PER_DAY', 'AVG_PER_WEEK', name='dataaggregationenum'),
               nullable=False,
               schema='equipments')
    op.add_column('thresholds', sa.Column('duration', sa.Integer(), nullable=True), schema='equipments')
    op.execute("UPDATE thresholds SET duration = duration_min")

    op.drop_column('analytics', 'amount_data_min', schema='equipments')
    op.drop_column('thresholds', 'duration_min', schema='equipments')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('thresholds', sa.Column('duration_min', sa.INTEGER(), autoincrement=False, nullable=True), schema='equipments')
    op.execute("UPDATE thresholds SET duration_min = duration")

    op.add_column('analytics', sa.Column('amount_data_min', sa.INTEGER(), autoincrement=False, nullable=True), schema='equipments')
    op.execute("UPDATE analytics SET amount_data_min = amount_data")
    op.alter_column('analytics', 'amount_data_min', existing_type=sa.Integer(), nullable=False, schema='equipments')

    op.alter_column('analytics', 'data_aggregation',
               existing_type=postgresql.ENUM('NO_AGGREGATION', 'AVG_PER_MICROSECOND', 'AVG_PER_MILLISECOND', 'AVG_PER_SECOND', 'AVG_PER_MINUTE', 'AVG_PER_HOUR', 'AVG_PER_DAY', 'AVG_PER_WEEK', name='dataaggregationenum'),
               nullable=True,
               schema='equipments')
    op.alter_column('analytics', 'active',
               existing_type=sa.BOOLEAN(),
               nullable=True,
               schema='equipments')

    op.drop_column('thresholds', 'duration', schema='equipments')
    op.drop_column('analytics', 'amount_data', schema='equipments')
    op.drop_constraint(None, 'alarms', schema='equipments', type_='foreignkey')
    op.create_foreign_key('alarms_type_name_color_fkey', 'alarms', 'alarm_configs', ['type_name', 'color'], ['type_name', 'color'], source_schema='equipments', referent_schema='equipments')
    # ### end Alembic commands ###

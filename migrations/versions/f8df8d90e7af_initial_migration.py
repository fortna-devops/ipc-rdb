"""initial migration

Revision ID: f8df8d90e7af
Revises: 
Create Date: 2021-06-23 12:41:13.542621

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'f8df8d90e7af'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # asset_types table
    op.create_table('asset_types',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('manufacturer', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('model', sa.String(), nullable=True),

        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table asset_types OWNER to mhs')

    # alarm_types table
    op.create_table('alarm_types',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.PrimaryKeyConstraint('name')
    )
    op.execute('alter table alarm_types OWNER to mhs')

    # alert_users table
    op.create_table('alert_users',
        sa.Column('email', sa.String(), nullable=False),
        sa.Column('phone', sa.String(), nullable=True),
        sa.Column('active', sa.Boolean(), nullable=False, default=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.PrimaryKeyConstraint('email')
    )
    op.execute('alter table alert_users OWNER to mhs')

    # component_types table
    op.create_table('component_types',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('manufacturer', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('model', sa.String(), nullable=True),

        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table component_types OWNER to mhs')

    # data_source_types table
    op.create_table('data_source_types',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('manufacturer', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('model', sa.String(), nullable=True),

        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table data_source_types OWNER to mhs')

    # errors table
    op.create_table('errors',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('description', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.PrimaryKeyConstraint('name')
    )
    op.execute('alter table errors OWNER to mhs')

    # facility_areas table
    op.create_table('facility_areas',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False),

        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table facility_areas OWNER to mhs')

    # gateways table
    op.create_table('gateways',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('cognito_client_id', sa.String(), nullable=True),
        sa.Column('description', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('heartbeat_timestamp', sa.DateTime(), nullable=True),
        sa.Column('last_message_status', sa.String(), nullable=True),
        sa.Column('last_message_timestamp', sa.DateTime(), nullable=True),

        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table gateways OWNER to mhs')

    # kpi_groups table
    op.create_table('kpi_groups',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('display_name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table kpi_groups OWNER to mhs')

    # operational_codes table
    op.create_table('operational_codes',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('value', sa.BigInteger(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table operational_codes OWNER to mhs')
   
    # threshold_levels table
    op.create_table('threshold_levels',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('color', sa.Enum('UNDEFINED', 'GREEN', 'YELLOW', 'RED', name='colorsenum'), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.PrimaryKeyConstraint('name')
    )
    op.execute('alter table threshold_levels OWNER to mhs')

    # threshold_types table
    op.create_table('threshold_types',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.PrimaryKeyConstraint('name')
    )
    op.execute('alter table threshold_types OWNER to mhs')

    # warnings table
    op.create_table('warnings',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('description', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.PrimaryKeyConstraint('name')
    )
    op.execute('alter table warnings OWNER to mhs')

    # alarm_configs table
    op.create_table('alarm_configs',
        sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False),
        sa.Column('type_name', sa.String(), nullable=False),
        sa.Column('color', sa.Enum('UNDEFINED', 'GREEN', 'YELLOW', 'RED', name='colorsenum'), nullable=False),
        sa.Column('weight', sa.Integer(), server_default='1', nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.ForeignKeyConstraint(['type_name'], ['alarm_types.name'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('type_name', 'color')
    )
    op.execute('alter table alarm_configs OWNER to mhs')

    # alert_roles table
    op.create_table('alert_roles',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('role', sa.Enum('developer', 'support', 'customer', name='alertrolesenum'), nullable=False),
        sa.Column('alert_user_email', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.ForeignKeyConstraint(['alert_user_email'], ['alert_users.email'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('alert_user_email', 'role')
    )
    op.execute('alter table alert_roles OWNER to mhs')

    # assets table
    op.create_table('assets',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('facility_area_id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('install_date', sa.DateTime(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False),
        sa.Column('type_id', sa.Integer(), nullable=False),

        sa.ForeignKeyConstraint(['facility_area_id'], ['facility_areas.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['type_id'], ['asset_types.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table assets OWNER to mhs')

    # asset_status_transitions table
    op.create_table('asset_status_transitions',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('asset_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('status', sa.Enum('ON', 'OFF', name='assetstatusesenum'), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('interval', sa.Interval(), server_default='0', nullable=False),

        sa.ForeignKeyConstraint(['asset_id'], ['assets.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('asset_status_transitions_asset_id_status_timestamp_desc_index',
        'asset_status_transitions', ['asset_id', 'status', sa.text('timestamp DESC')], unique=False
    )
    op.execute('alter table asset_status_transitions OWNER to mhs')

    # components table
    op.create_table('components',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('asset_id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('location', sa.String(), nullable=True),
        sa.Column('install_date', sa.DateTime(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False),
        sa.Column('type_id', sa.Integer(), nullable=False),

        sa.ForeignKeyConstraint(['asset_id'], ['assets.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['type_id'], ['component_types.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table components OWNER to mhs')

    # kpis table
    op.create_table('kpis',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('asset_id', sa.Integer(), nullable=False),
        sa.Column('kpi_group_id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('display_name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.ForeignKeyConstraint(['asset_id'], ['assets.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['kpi_group_id'], ['kpi_groups.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table kpis OWNER to mhs')

    # component_health table
    op.create_table('component_health',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('component_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('health', sa.Float(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.ForeignKeyConstraint(['component_id'], ['components.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('component_id', 'timestamp')
    )
    op.execute('alter table component_health OWNER to mhs')

    # data_sources table
    op.create_table('data_sources',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('component_id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('location', sa.String(), nullable=True),
        sa.Column('install_date', sa.DateTime(), nullable=True),
        sa.Column('address', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('type_id', sa.Integer(), nullable=False),

        sa.ForeignKeyConstraint(['component_id'], ['components.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['type_id'], ['data_source_types.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table data_sources OWNER to mhs')

    # alert_black_list table
    op.create_table('alert_black_list',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('alert_user_email', sa.String(), nullable=False),
        sa.Column('data_source_id', sa.Integer(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.ForeignKeyConstraint(['alert_user_email'], ['alert_users.email'], ),
        sa.ForeignKeyConstraint(['data_source_id'], ['data_sources.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table alert_black_list OWNER to mhs')

    # data_sources_gateways_association table
    op.create_table('data_sources_gateways_association',
        sa.Column('data_source_id', sa.Integer(), nullable=False),
        sa.Column('gateway_id', sa.Integer(), nullable=False),
        sa.Column('address', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.ForeignKeyConstraint(['data_source_id'], ['data_sources.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['gateway_id'], ['gateways.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('data_source_id', 'gateway_id')
    )
    op.execute('alter table data_sources_gateways_association OWNER to mhs')

    # metrics table
    op.create_table('metrics',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('data_source_id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('display_name', sa.String(), nullable=False),
        sa.Column('units', sa.String(), nullable=True),
        sa.Column('min_value', sa.Float(), nullable=True),
        sa.Column('max_value', sa.Float(), nullable=True),
        sa.Column('raw_data_table', sa.String(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('to_analyze', sa.Boolean(), server_default='TRUE', nullable=False),
        sa.Column('to_display', sa.Boolean(), server_default='TRUE', nullable=False),
        sa.Column('calculated_data_table', sa.String(), nullable=True),

        sa.CheckConstraint('raw_data_table IN (NULL, NULL, NULL) OR calculated_data_table IN (NULL, NULL, NULL)', name='data_table_check'),
        sa.ForeignKeyConstraint(['data_source_id'], ['data_sources.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table metrics OWNER to mhs')

    # alarms table
    op.create_table('alarms',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('started_at', sa.DateTime(), nullable=False),
        sa.Column('ended_at', sa.DateTime(), nullable=False),
        sa.Column('type_name', sa.String(), nullable=False),
        sa.Column('color', sa.Enum('UNDEFINED', 'GREEN', 'YELLOW', 'RED', name='colorsenum'), nullable=False),
        sa.Column('trigger_value', postgresql.DOUBLE_PRECISION(), nullable=False),
        sa.Column('trigger_criteria', postgresql.DOUBLE_PRECISION(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('static_description', sa.String(), nullable=True),

        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['type_name', 'color'], ['alarm_configs.type_name', 'alarm_configs.color'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['type_name'], ['alarm_types.name'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('create extension if not exists btree_gist')
    op.create_index('alarms_tsrange_metric_id_type_name_started_at_index', 'alarms',
                    [sa.text("tsrange(started_at, ended_at, '[]')"), 'metric_id', 'type_name', 'started_at'],
                    postgresql_using='gist')
    op.execute('alter table alarms OWNER to mhs')

    # calculated_float_data table
    op.create_table('calculated_float_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.REAL(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('calculated_float_data_metric_id_timestamp_minute_flags_index', 'calculated_float_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('calculated_float_data_metric_id_timestamp_hour_flags_index', 'calculated_float_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    op.execute('alter table calculated_float_data OWNER to mhs')

    # calculated_int_data table
    op.create_table('calculated_int_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.Integer(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('calculated_int_data_metric_id_timestamp_minute_flags_index', 'calculated_int_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('calculated_int_data_metric_id_timestamp_hour_flags_index', 'calculated_int_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    op.execute('alter table calculated_int_data OWNER to mhs')

    # calculated_string_data table
    op.create_table('calculated_string_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),

        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('calculated_string_data_metric_id_timestamp_minute_flags_index', 'calculated_string_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('calculated_string_data_metric_id_timestamp_hour_flags_index', 'calculated_string_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    op.execute('alter table calculated_string_data OWNER to mhs')

    # metrics_kpis_association table
    op.create_table('metrics_kpis_association',
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('kpis_id', sa.Integer(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),

        sa.ForeignKeyConstraint(['kpis_id'], ['kpis.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('metric_id', 'kpis_id')
    )
    op.execute('alter table metrics_kpis_association OWNER to mhs')

    # raw_float_data table
    op.create_table('raw_float_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.REAL(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('operational_code_id', sa.Integer(), nullable=False),

        sa.ForeignKeyConstraint(['operational_code_id'], ['operational_codes.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('raw_float_data_metric_id_timestamp_minute_flags_index', 'raw_float_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('raw_float_data_metric_id_timestamp_hour_flags_index', 'raw_float_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    op.execute('alter table raw_float_data OWNER to mhs')

    # raw_int_data table
    op.create_table('raw_int_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.Integer(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('operational_code_id', sa.Integer(), nullable=False),

        sa.ForeignKeyConstraint(['operational_code_id'], ['operational_codes.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('raw_int_data_metric_id_timestamp_minute_flags_index', 'raw_int_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('raw_int_data_metric_id_timestamp_hour_flags_index', 'raw_int_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    op.execute('alter table raw_int_data OWNER to mhs')

    # raw_string_data table
    op.create_table('raw_string_data',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
        sa.Column('value', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('operational_code_id', sa.Integer(), nullable=False),

        sa.ForeignKeyConstraint(['operational_code_id'], ['operational_codes.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('raw_string_data_metric_id_timestamp_minute_flags_index', 'raw_string_data',
                    ['metric_id', sa.text("date_trunc('minute', timestamp)")])
    op.create_index('raw_string_data_metric_id_timestamp_hour_flags_index', 'raw_string_data',
                    ['metric_id', sa.text("date_trunc('hour', timestamp)")])
    op.execute('alter table raw_string_data OWNER to mhs')

    # thresholds table
    op.create_table('thresholds',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=False),
        sa.Column('operational_code_id', sa.Integer(), nullable=False),
        sa.Column('type_name', sa.String(), nullable=False),
        sa.Column('level_name', sa.String(), nullable=False),
        sa.Column('value', sa.Float(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('active', sa.Boolean(), nullable=False),

        sa.ForeignKeyConstraint(['level_name'], ['threshold_levels.name'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['operational_code_id'], ['operational_codes.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['type_name'], ['alarm_types.name'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute('alter table thresholds OWNER to mhs')


def downgrade():
    # drop indexes
    op.drop_index('asset_status_transitions_asset_id_status_timestamp_desc_index', table_name='asset_status_transitions')
    op.drop_index('alarms_tsrange_metric_id_type_name_started_at_index', table_name='alarms')
    op.drop_index('calculated_float_data_metric_id_timestamp_minute_flags_index', table_name='calculated_float_data')
    op.drop_index('calculated_float_data_metric_id_timestamp_hour_flags_index', table_name='calculated_float_data')
    op.drop_index('calculated_string_data_metric_id_timestamp_minute_flags_index', table_name='calculated_string_data')
    op.drop_index('calculated_string_data_metric_id_timestamp_hour_flags_index', table_name='calculated_string_data')
    op.drop_index('calculated_int_data_metric_id_timestamp_minute_flags_index', table_name='calculated_int_data')
    op.drop_index('calculated_int_data_metric_id_timestamp_hour_flags_index', table_name='calculated_int_data')
    op.drop_index('raw_float_data_metric_id_timestamp_minute_flags_index', table_name='raw_float_data')
    op.drop_index('raw_float_data_metric_id_timestamp_hour_flags_index', table_name='raw_float_data')
    op.drop_index('raw_string_data_metric_id_timestamp_minute_flags_index', table_name='raw_string_data')
    op.drop_index('raw_string_data_metric_id_timestamp_hour_flags_index', table_name='raw_string_data')
    op.drop_index('raw_int_data_metric_id_timestamp_minute_flags_index', table_name='raw_int_data')
    op.drop_index('raw_int_data_metric_id_timestamp_hour_flags_index', table_name='raw_int_data')

    # drop tables
    op.drop_table('thresholds')
    op.drop_table('raw_string_data')
    op.drop_table('raw_int_data')
    op.drop_table('raw_float_data')
    op.drop_table('metrics_kpis_association')
    op.drop_table('calculated_string_data')
    op.drop_table('calculated_int_data')
    op.drop_table('calculated_float_data')
    op.drop_table('alarms')
    op.drop_table('metrics')
    op.drop_table('data_sources_gateways_association')
    op.drop_table('alert_black_list')
    op.drop_table('data_sources')
    op.drop_table('component_health')
    op.drop_table('kpis')
    op.drop_table('components')
    op.drop_table('asset_status_transitions')
    op.drop_table('assets')
    op.drop_table('alert_roles')
    op.drop_table('alarm_configs')
    op.drop_table('warnings')
    op.drop_table('threshold_types')
    op.drop_table('threshold_levels')
    op.drop_table('operational_codes')
    op.drop_table('kpi_groups')
    op.drop_table('gateways')
    op.drop_table('facility_areas')
    op.drop_table('errors')
    op.drop_table('data_source_types')
    op.drop_table('component_types')
    op.drop_table('alert_users')
    op.drop_table('alarm_types')
    op.drop_table('asset_types')

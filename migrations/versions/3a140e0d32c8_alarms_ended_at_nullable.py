"""alarms_ended_at_nullable

Revision ID: 3a140e0d32c8
Revises: adedbfb7d84a
Create Date: 2021-11-08 16:29:36.652951

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '3a140e0d32c8'
down_revision = 'adedbfb7d84a'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('alarms', 'ended_at',
               existing_type=postgresql.TIMESTAMP(),
               nullable=True,
               schema='equipments')


def downgrade():
    op.alter_column('alarms', 'ended_at',
               existing_type=postgresql.TIMESTAMP(),
               nullable=False,
               schema='equipments')

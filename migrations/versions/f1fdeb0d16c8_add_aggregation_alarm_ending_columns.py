"""add aggregation, alarm_ending columns

Revision ID: f1fdeb0d16c8
Revises: 5857cfadb9d1
Create Date: 2022-02-10 13:24:43.849976

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f1fdeb0d16c8'
down_revision = '5857cfadb9d1'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('thresholds', sa.Column('aggregation', sa.Enum('NO_AGGREGATION', 'AVG_PER_MICROSECOND', 'AVG_PER_MILLISECOND', 'AVG_PER_SECOND', 'AVG_PER_MINUTE', 'AVG_PER_HOUR', name='dataaggregationenum'), nullable=True), schema='equipments')
    op.add_column('thresholds', sa.Column('alarm_ending', sa.Integer(), nullable=True), schema='equipments')


def downgrade():
    op.drop_column('thresholds', 'alarm_ending', schema='equipments')
    op.drop_column('thresholds', 'aggregation', schema='equipments')

"""add logging table

Revision ID: 449d26874010
Revises: ac6752ba9288
Create Date: 2021-07-28 14:28:16.618734

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '449d26874010'
down_revision = 'ac6752ba9288'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('logging',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('timestamp', sa.DateTime(), nullable=False),
    sa.Column('module', sa.String(), nullable=False),
    sa.Column('level', sa.String(), nullable=False),
    sa.Column('message', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    schema='equipments'
    )

def downgrade():
    op.drop_table('logging', schema='equipments')

from logging.config import fileConfig
from configparser import ConfigParser

from sqlalchemy import engine_from_config
from sqlalchemy import pool

from alembic import context

#from ipc_rdb.models.base_model import BaseEquipmentsModel, BaseApplicationModel
from ipc_rdb.models.base_model import BaseSiteModel, BaseApplicationModel

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Configure credentials
credentials = ConfigParser()
credentials.read('credentials.ini')
config.set_main_option('username', credentials.get(config.config_ini_section, 'username'))
config.set_main_option('password', credentials.get(config.config_ini_section, 'password'))

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

target_metadata = [BaseSiteModel.metadata, BaseApplicationModel.metadata]
SCHEMA_NAME = 'equipments'
# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        include_schemas=True,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    Add support for 'dry-run'.
    https://stackoverflow.com/questions/51556996/do-a-dry-run-of-an-alembic-upgrade

    """
    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,

        # ensure the context will create a transaction
        # for backends that dont normally use transactional DDL.
        # note that ROLLBACK will not roll back DDL structures
        # on databases such as MySQL, as well as with SQLite
        # Python driver's default settings
        # transactional_ddl=True,
    )

    with connectable.connect() as connection:
        connection.execute(f'set search_path to {SCHEMA_NAME};')
        context.configure(
            connection=connection,
            include_schemas=True,
            target_metadata=target_metadata,
            version_table_schema=SCHEMA_NAME
        )

        connection.execute(f'CREATE SCHEMA IF NOT EXISTS {SCHEMA_NAME};')

        with context.begin_transaction() as transaction:
            context.run_migrations()

            # To do a dry-run, do:
            #   alembic -x dry-run upgrade head
            if 'dry-run' in context.get_x_argument():
                print('Dry-run succeeded; now rolling back transaction...')
                transaction.rollback()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()

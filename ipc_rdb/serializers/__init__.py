from .base import BaseSerializer
from .model import ModelDictSerializer


def serialize(obj, cls=ModelDictSerializer):
    return cls().serialize(obj)

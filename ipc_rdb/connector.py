import os
import logging
from typing import Optional, Dict, Any, List

from sqlalchemy import create_engine, event
from sqlalchemy.engine import interfaces, Engine
from sqlalchemy.orm import sessionmaker, scoped_session, Session


from .session import Session


logger = logging.getLogger()


ENV_VAR_PREFIX = 'IPC_RDB_'
DEFAULT_PROTOCOL = 'postgresql'
DEFAULT_PORT = '5432'
DEFAULT_ENGINE_OPTIONS: Dict[str, Any] = {
    'pool_pre_ping': True
}


__all__ = ['Connector']


def get_env_var(name: str, options: Dict[str, str], default: Optional[str] = None) -> Optional[str]:
    var = options.get(name)
    if not var:
        var = os.getenv(f'{ENV_VAR_PREFIX}{name.upper()}')
    if not var:
        var = default
    return var


class Connector:
    """
    Connector for establishing connection to a Postgres database
    """

    def __init__(self, debug: bool = False, **options):
        self._debug = debug
        self._protocol = get_env_var('protocol', options, DEFAULT_PROTOCOL)
        self._host = get_env_var('host', options)
        self._port = get_env_var('port', options, DEFAULT_PORT)
        self._username = get_env_var('username', options)
        self._password = get_env_var('password', options)
        self._engine_options: Dict[str, Any] = options.get('engine_options', DEFAULT_ENGINE_OPTIONS)
        self._session_makers: Dict[str, scoped_session] = {}

    def _get_url(self, database: str) -> str:
        return f'{self._protocol}://{self._host}:{self._port}/{database}'

    def _get_credentials(self) -> Dict[str, Optional[str]]:
        return {'user': self._username, 'password': self._password}

    def _get_session(self, database: str) -> Session:
        session_maker = sessionmaker(bind=self.get_engine(database), class_=Session)
        self._session_makers[database] = scoped_session(session_maker)
        return self._session_makers[database]()

    @staticmethod
    def _set_schema(session: Session, schema: str):
        logger.debug(f'Set search path to "{schema}"')
        session.execute(f'SET search_path TO {schema}')

    def _on_do_connect(self, _dialect: interfaces.Dialect, _conn_rec: Any,
                       _cargs: List[Any], cparams: Dict[str, Any]) -> None:
        cparams.update(self._get_credentials())

    def get_engine(self, database: str) -> Engine:
        url = self._get_url(database)
        engine = create_engine(url, echo=self._debug, **self._engine_options)
        event.listen(engine, 'do_connect', self._on_do_connect)
        return engine

    def get_session(self, database: str, schema: str = 'equipments') -> Session:
        session = self._get_session(database)
        self._set_schema(session, schema)
        return session

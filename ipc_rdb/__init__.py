import sys


VERSION = (1, 0, 0)
__version__ = '.'.join(str(v) for v in VERSION)


# It will be better just to remove those imports.
# But it will require to update every place in project that use this lib.
if 'setup.py' not in sys.argv[0]:
    # from ipc_rdb.config_manager import ConfigManager
    from ipc_rdb.connector import Connector
    from ipc_rdb.services import *

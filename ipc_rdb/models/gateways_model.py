from typing import TYPE_CHECKING

from sqlalchemy import Column, String, DateTime
from sqlalchemy.orm import relationship

from .mixins import IdMixin, TableNameMixin, TimestampedMixin
from .base_model import BaseSiteModel


__all__ = ['GatewaysModel']


if TYPE_CHECKING:
    from . import DataSourcesModel, DataSourcesGatewaysAssociationModel


class GatewaysModel(TableNameMixin, IdMixin, TimestampedMixin, BaseSiteModel):
    name = Column(String, nullable=False)
    cognito_client_id = Column(String)
    description = Column(String)
    heartbeat_timestamp = Column(DateTime)
    last_message_timestamp = Column(DateTime)
    last_message_status = Column(String)

    data_source_associations = relationship('DataSourcesGatewaysAssociationModel', back_populates='gateway')

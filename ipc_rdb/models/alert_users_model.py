from typing import TYPE_CHECKING

from sqlalchemy import Column, String, Boolean
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, TimestampedMixin
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import AlertRolesModel, AlertBlackListModel


__all__ = ['AlertUsersModel']


class AlertUsersModel(TableNameMixin, TimestampedMixin, BaseSiteModel):
    email = Column(String, primary_key=True)
    phone = Column(String)
    active = Column(Boolean, nullable=False, default=True)

    alert_roles = relationship('AlertRolesModel', back_populates='alert_user')
    alert_black_list = relationship('AlertBlackListModel', back_populates='alert_user')

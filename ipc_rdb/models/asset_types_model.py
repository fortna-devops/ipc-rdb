from typing import TYPE_CHECKING

from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, TimestampedMixin, IdMixin


if TYPE_CHECKING:
    from . import AssetsModel


__all__ = ['AssetTypesModel']


class AssetTypesModel(TableNameMixin, IdMixin, TimestampedMixin, BaseSiteModel):
    name = Column(String, nullable=False)
    manufacturer = Column(String)
    model = Column(String)

    assets = relationship('AssetsModel', back_populates='type')

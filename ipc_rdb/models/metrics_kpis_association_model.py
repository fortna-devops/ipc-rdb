from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, TimestampedMixin

if TYPE_CHECKING:
    from . import MetricsModel, KpisModel


__all__ = ['MetricsKpisAssociationModel']


class MetricsKpisAssociationModel(TableNameMixin, TimestampedMixin, BaseSiteModel):
    metric_id = Column(Integer, ForeignKey('metrics.id', ondelete='RESTRICT'),
                       nullable=False, primary_key=True)
    kpis_id = Column(Integer, ForeignKey('kpis.id', ondelete='RESTRICT'),
                     nullable=False, primary_key=True)

    metric = relationship('MetricsModel')
    kpi = relationship('KpisModel', back_populates="metrics_kpis_associations")

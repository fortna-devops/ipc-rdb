from typing import TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, String, Enum, Integer
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, TimestampedMixin, ToDisplayMixin
from .enums import ColorsEnum
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import AlarmsModel, AnalyticsModel


__all__ = ['AlarmConfigsModel']


class AlarmConfigsModel(TableNameMixin, TimestampedMixin, ToDisplayMixin, BaseSiteModel):
    """
    Stores configurations for each category of alarm types.

    `weight` can be used when combining different alarms based on type and
    color (e.g. to produce an aggregate health rating).
    """
    analytic_name = Column(String, ForeignKey('analytics.name', ondelete='RESTRICT'), nullable=False, primary_key=True)
    color = Column(Enum(ColorsEnum), nullable=False, primary_key=True, unique=True)
    weight = Column(Integer, nullable=False, server_default='1')

    analytic = relationship('AnalyticsModel', back_populates='alarm_configs')

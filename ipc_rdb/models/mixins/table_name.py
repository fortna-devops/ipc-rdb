import re

from sqlalchemy.ext.declarative import declared_attr

__all__ = ['TableNameMixin']

TABLE_NAME_PATTERN = re.compile(r'([A-Z]+(?=[A-Z])(?!=[a-z])|[A-Z][a-z]+)')


class TableNameMixin:

    @declared_attr
    def __tablename__(cls):  # pylint: disable=E0213
        name = cls.__name__.replace('Model', '')
        name_parts = re.findall(TABLE_NAME_PATTERN, name)
        return '_'.join([p.lower() for p in name_parts])

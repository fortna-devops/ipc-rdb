from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, IdMixin, TimestampedMixin


if TYPE_CHECKING:
    from . import MetricsModel, ComponentsModel, DataSourceTypesModel, GatewaysModel,\
        DataSourcesGatewaysAssociationModel


__all__ = ['DataSourcesModel']


class DataSourcesModel(TableNameMixin, IdMixin, TimestampedMixin, BaseSiteModel):
    component_id = Column(Integer, ForeignKey('components.id', ondelete='RESTRICT'), nullable=False)
    type_id = Column(Integer, ForeignKey('data_source_types.id', ondelete='RESTRICT'), nullable=False)
    name = Column(String, nullable=False)
    location = Column(String)
    install_date = Column(DateTime)
    address = Column(String)

    metrics = relationship('MetricsModel', back_populates='data_source')
    component = relationship('ComponentsModel', back_populates='data_sources')
    type = relationship('DataSourceTypesModel', back_populates='data_sources')

    """
    sqlalchemy.exc.NoForeignKeysError: Could not determine join condition between parent/child tables on
    relationship DataSourcesModel.gateways - there are no foreign keys linking these tables.
    """
    gateway_associations = relationship('DataSourcesGatewaysAssociationModel', back_populates='data_source')

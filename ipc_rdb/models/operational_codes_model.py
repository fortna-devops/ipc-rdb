from sqlalchemy import BigInteger, Column

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, IdMixin, TimestampedMixin


__all__ = ['OperationalCodesModel']


class OperationalCodesModel(TableNameMixin, IdMixin, TimestampedMixin, BaseSiteModel):
    value = Column(BigInteger, nullable=False)

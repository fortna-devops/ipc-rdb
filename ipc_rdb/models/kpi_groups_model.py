from typing import TYPE_CHECKING

from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, TimestampedMixin, IdMixin
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import KpisModel


__all__ = ['KpiGroupsModel']


class KpiGroupsModel(TableNameMixin, TimestampedMixin, IdMixin, BaseSiteModel):

    display_name = Column(String, nullable=False)

    kpis = relationship('KpisModel', back_populates="kpi_group")

from typing import TYPE_CHECKING

from sqlalchemy import ForeignKey, Column, Integer, String
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, TimestampedMixin, IdMixin
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import AssetsModel, KpiGroupsModel, MetricsModel, MetricsKpisAssociationModel


__all__ = ['KpisModel']


class KpisModel(TableNameMixin, IdMixin, TimestampedMixin, BaseSiteModel):

    asset_id = Column(Integer, ForeignKey('assets.id', ondelete='RESTRICT'), nullable=False)
    kpi_group_id = Column(Integer, ForeignKey('kpi_groups.id', ondelete='RESTRICT'), nullable=False)

    name = Column(String, nullable=False)
    display_name = Column(String, nullable=False)

    asset = relationship('AssetsModel')
    kpi_group = relationship('KpiGroupsModel', back_populates="kpis")

    metrics_kpis_associations = relationship('MetricsKpisAssociationModel', back_populates="kpi")

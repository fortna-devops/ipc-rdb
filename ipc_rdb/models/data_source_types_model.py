from typing import TYPE_CHECKING

from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, TimestampedMixin, IdMixin


if TYPE_CHECKING:
    from . import DataSourcesModel


__all__ = ['DataSourceTypesModel']


class DataSourceTypesModel(TableNameMixin, IdMixin, TimestampedMixin, BaseSiteModel):

    name = Column(String, nullable=False)
    manufacturer = Column(String)
    model = Column(String)

    data_sources = relationship('DataSourcesModel', back_populates='type')

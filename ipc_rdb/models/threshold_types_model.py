from typing import TYPE_CHECKING

from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, UpdatedAtMixin

if TYPE_CHECKING:
    from . import ThresholdsModel

__all__ = ['ThresholdTypesModel']


class ThresholdTypesModel(TableNameMixin, UpdatedAtMixin, BaseSiteModel):
    """
    Contains thresholds types that are used to denote what type of threshold it is.

    `name` represents a type of threshold (e.g. 'metric_distribution', 'iso',
    'ml', etc.)
    """
    name = Column(String, nullable=False, primary_key=True)

    thresholds = relationship('ThresholdsModel')

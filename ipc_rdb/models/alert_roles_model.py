from typing import TYPE_CHECKING

from sqlalchemy import Column, Enum, UniqueConstraint, String, ForeignKey
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, CreatedAtMixin, IdMixin
from .enums import AlertRolesEnum
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import AlertUsersModel


__all__ = ['AlertRolesModel']


class AlertRolesModel(TableNameMixin, IdMixin, CreatedAtMixin, BaseSiteModel):
    __table_args__ = (
        UniqueConstraint('alert_user_email', 'role'),
    )

    role = Column(Enum(AlertRolesEnum), nullable=False)
    alert_user_email = Column(String, ForeignKey('alert_users.email'), nullable=False)

    alert_user = relationship('AlertUsersModel', back_populates='alert_roles')

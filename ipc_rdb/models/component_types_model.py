from typing import TYPE_CHECKING

from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, TimestampedMixin, IdMixin


if TYPE_CHECKING:
    from . import ComponentsModel


__all__ = ['ComponentTypesModel']


class ComponentTypesModel(TableNameMixin, IdMixin, TimestampedMixin, BaseSiteModel):
    name = Column(String, nullable=False)
    manufacturer = Column(String)
    model = Column(String)

    components = relationship('ComponentsModel', back_populates='type')

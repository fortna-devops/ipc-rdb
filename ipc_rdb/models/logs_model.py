from sqlalchemy import Column, String, DateTime

from .mixins import IdMixin, TableNameMixin
from .base_model import BaseSiteModel

__all__ = ['LogsModel']

class LogsModel(TableNameMixin, IdMixin, BaseSiteModel):
    timestamp = Column(DateTime, index=True, nullable=False)
    module = Column(String, nullable=False)
    level = Column(String, nullable=False)
    message = Column(String, nullable=False)

from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, IdMixin, CreatedAtMixin
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import AlertUsersModel, DataSourcesModel


__all__ = ['AlertBlackListModel']


class AlertBlackListModel(TableNameMixin, IdMixin, CreatedAtMixin, BaseSiteModel):
    alert_user_email = Column(String, ForeignKey('alert_users.email'), nullable=False)
    data_source_id = Column(Integer, ForeignKey('data_sources.id'), nullable=False)

    alert_user = relationship('AlertUsersModel', back_populates='alert_black_list')
    data_source = relationship('DataSourcesModel')

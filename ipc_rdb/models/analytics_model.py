from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, String, Enum
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Boolean

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, UpdatedAtMixin
from .enums import DataAggregationEnum


if TYPE_CHECKING:
    from . import AlarmConfigsModel

__all__ = ['AnalyticsModel']


class AnalyticsModel(TableNameMixin, UpdatedAtMixin, BaseSiteModel):
    name = Column(String, primary_key=True)
    active = Column(Boolean, nullable=False, default=False)
    amount_data = Column(Integer, nullable=False, default=10)
    data_aggregation = Column(Enum(DataAggregationEnum), nullable=False)

    alarm_configs = relationship('AlarmConfigsModel', back_populates='analytic')

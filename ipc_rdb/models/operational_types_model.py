from typing import TYPE_CHECKING

from sqlalchemy import String, Column
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import BigInteger

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, IdMixin, UpdatedAtMixin

if TYPE_CHECKING:
    from . import ThresholdsModel
    from . import OperationalGroupsModel

__all__ = ['OperationalTypesModel']


class OperationalTypesModel(TableNameMixin, IdMixin, UpdatedAtMixin, BaseSiteModel):
    name = Column(String, nullable=False)
    value = Column(BigInteger, nullable=False)

    thresholds = relationship('ThresholdsModel', back_populates='operational_type')
    groups = relationship('OperationalGroupsModel', back_populates='type')

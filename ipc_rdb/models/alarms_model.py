from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, ForeignKey, DateTime, String, ForeignKeyConstraint, Index, Enum, func
from sqlalchemy.dialects.postgresql import DOUBLE_PRECISION
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, BigIdMixin, TimestampedMixin
from .enums import ColorsEnum
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import MetricsModel, AlarmConfigsModel, AnalyticsModel, AcknowledgmentsModel


__all__ = ['AlarmsModel']


class AlarmsModel(TableNameMixin, BigIdMixin, TimestampedMixin, BaseSiteModel):

    @declared_attr
    def __table_args__(cls):  # pylint: disable=E0213
        return (
            ForeignKeyConstraint(
                ['analytic_name', 'color'],
                ['alarm_configs.analytic_name', 'alarm_configs.color'],
                onupdate='CASCADE',
                ondelete='RESTRICT'
            ),
            Index(
                'idx_started_at_metric_id_analytic_name',
                cls.started_at, cls.analytic_name, cls.metric_id,
                postgresql_using='btree'
            ),
        )

    metric_id = Column(Integer, ForeignKey('metrics.id', ondelete='RESTRICT'), nullable=False)
    analytic_name = Column(String, ForeignKey('alarm_configs.analytic_name', ondelete='RESTRICT'), nullable=False)
    color = Column(Enum(ColorsEnum), nullable=False)
    started_at = Column(DateTime, nullable=False)
    ended_at = Column(DateTime, nullable=True)
    trigger_value = Column(DOUBLE_PRECISION, nullable=False)
    trigger_criteria = Column(DOUBLE_PRECISION, nullable=False)
    static_description = Column(String)

    metric = relationship('MetricsModel', back_populates='alarms')

    # one Alarm may have many Acknowledgements
    alarm_acks = relationship('AcknowledgmentsModel', back_populates='alarm')

    @property
    def description(self) -> str:

        if self.static_description:
            return self.static_description

        exceed = 'exceeded'
        exceed_value = self.trigger_value - self.trigger_criteria
        if self.trigger_value < self.trigger_criteria:
            exceed = 'was less than'
            exceed_value = self.trigger_criteria - self.trigger_value

        units = ''
        if self.metric.units is not None:
            units = f' {self.metric.units}'

        # if alarms has ended_at value, it is no longer active
        if self.ended_at:
            duration = (self.ended_at - self.started_at).total_seconds() / 60.0
            alarm_status = "for"

        # if alarms has ended_at null, it is continuing
        else:
            duration = (func.now() - self.started_at).total_seconds() / 60.0
            alarm_status = "since"

        duration_units = 'minutes'
        if duration > 60:
            duration /= 60
            duration_units = 'hours'

        return f"{self.metric.display_name} {exceed} {self.trigger_criteria}{units} by an average of " \
               f"{exceed_value:.2f}{units} {alarm_status} {duration:.1f} consecutive {duration_units}"

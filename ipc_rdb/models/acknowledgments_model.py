from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, IdMixin, TimestampedMixin
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import AlarmsModel


__all__ = ['AcknowledgmentsModel']


class AcknowledgmentsModel(TableNameMixin, IdMixin, TimestampedMixin, BaseSiteModel):

    alarm_id = Column(Integer, ForeignKey('alarms.id', ondelete='CASCADE'))
    work_order_id = Column(String, nullable=True)
    work_order_lead = Column(String, nullable=True)
    downtime_start = Column(DateTime, nullable=True)
    downtime_schedule = Column(String, nullable=True)
    parts_replaced = Column(String, nullable=True)
    comments = Column(String, nullable=True)

    #One Alarm may have many Acknowledgements
    alarm = relationship('AlarmsModel', back_populates='alarm_acks')

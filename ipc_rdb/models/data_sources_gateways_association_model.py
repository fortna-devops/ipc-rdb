from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, TimestampedMixin

if TYPE_CHECKING:
    from . import DataSourcesModel, GatewaysModel


__all__ = ['DataSourcesGatewaysAssociationModel']


class DataSourcesGatewaysAssociationModel(TableNameMixin, TimestampedMixin, BaseSiteModel):
    data_source_id = Column(Integer, ForeignKey('data_sources.id', ondelete='RESTRICT'),
                            nullable=False, primary_key=True)
    gateway_id = Column(Integer, ForeignKey('gateways.id', ondelete='RESTRICT'),
                        nullable=False, primary_key=True)
    address = Column(String)

    data_source = relationship('DataSourcesModel', back_populates='gateway_associations')
    gateway = relationship('GatewaysModel', back_populates='data_source_associations')

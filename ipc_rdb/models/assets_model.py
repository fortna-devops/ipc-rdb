from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, IdMixin, TimestampedMixin, ToDisplayMixin


if TYPE_CHECKING:
    from . import ComponentsModel, FacilityAreasModel, AssetTypesModel, AssetStatusTransitionsModel


__all__ = ['AssetsModel']


class AssetsModel(TableNameMixin, IdMixin, TimestampedMixin, ToDisplayMixin, BaseSiteModel):
    facility_area_id = Column(Integer, ForeignKey('facility_areas.id', ondelete='RESTRICT'), nullable=False)
    type_id = Column(Integer, ForeignKey('asset_types.id', ondelete='RESTRICT'), nullable=False)
    name = Column(String, nullable=False)
    install_date = Column(DateTime)

    facility_area = relationship('FacilityAreasModel', back_populates='assets')
    components = relationship('ComponentsModel', back_populates='asset')
    type = relationship('AssetTypesModel', back_populates='assets')
    status_transitions = relationship('AssetStatusTransitionsModel', back_populates='asset')

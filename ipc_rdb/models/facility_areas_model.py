from typing import TYPE_CHECKING

from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, IdMixin, TimestampedMixin, ToDisplayMixin


if TYPE_CHECKING:
    from . import AssetsModel


__all__ = ['FacilityAreasModel']


class FacilityAreasModel(TableNameMixin, IdMixin, TimestampedMixin, ToDisplayMixin, BaseSiteModel):
    name = Column(String, nullable=False)

    assets = relationship('AssetsModel', back_populates='facility_area')

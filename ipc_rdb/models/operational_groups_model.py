from typing import TYPE_CHECKING

from sqlalchemy import Integer, Column, ForeignKey
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, IdMixin, UpdatedAtMixin

if TYPE_CHECKING:
    from . import OperationalCodesModel
    from . import OperationalTypesModel

__all__ = ['OperationalGroupsModel']


class OperationalGroupsModel(TableNameMixin, IdMixin, UpdatedAtMixin, BaseSiteModel):
    type_id = Column(Integer, ForeignKey('operational_types.id', ondelete='RESTRICT'), nullable=False)
    operational_code_id = Column(Integer, ForeignKey('operational_codes.id', ondelete='RESTRICT'), nullable=False)

    codes = relationship('OperationalCodesModel')
    type = relationship('OperationalTypesModel', back_populates='groups')

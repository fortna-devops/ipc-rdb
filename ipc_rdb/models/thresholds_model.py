from typing import TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, Integer, Float, String, Boolean, Enum
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, IdMixin, UpdatedAtMixin
from .base_model import BaseSiteModel
from .enums import DataAggregationEnum

if TYPE_CHECKING:
    from . import MetricsModel, ThresholdLevelsModel, OperationalTypesModel, AnalyticsModel


__all__ = ['ThresholdsModel']


class ThresholdsModel(TableNameMixin, IdMixin, UpdatedAtMixin, BaseSiteModel):
    metric_id = Column(Integer, ForeignKey('metrics.id', ondelete='RESTRICT'), nullable=False)
    analytic = Column(String, ForeignKey('analytics.name', ondelete='RESTRICT'), nullable=False)

    level_name = Column(String, ForeignKey('threshold_levels.name', ondelete='RESTRICT'), nullable=False)
    value = Column(Float, nullable=False)
    operational_type_id = Column(Integer, ForeignKey('operational_types.id', ondelete='RESTRICT'), nullable=False)
    active = Column(Boolean, nullable=False, default=True)
    alarm_desc = Column(String, nullable=True)
    violations = Column(Integer, nullable=False)
    samples = Column(Integer, nullable=True)
    duration = Column(Integer, nullable=True)
    aggregation = Column(Enum(DataAggregationEnum), nullable=True)
    alarm_ending = Column(Integer, nullable=True)

    metric = relationship('MetricsModel', back_populates='thresholds')
    level = relationship('ThresholdLevelsModel', back_populates='thresholds')
    operational_type = relationship('OperationalTypesModel', back_populates='thresholds')

from typing import Dict, Any, ClassVar
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Table, MetaData
from sqlalchemy.orm.mapper import Mapper


__all__ = ['BaseModel', 'BaseSiteModel', 'BaseApplicationModel', 'EQUIPMENTS_SCHEMA_NAME',
           'APPLICATION_SCHEMA_NAME']


EQUIPMENTS_SCHEMA_NAME = 'equipments'
APPLICATION_SCHEMA_NAME = 'application'


class BaseModel:

    __table__: ClassVar[Table]
    __mapper__: ClassVar[Mapper]

    def to_dict(self, include_properties: bool = False) -> Dict[str, Any]:
        """
        Serialize SQLAlchemy model to python dict
        Based on https://gist.github.com/alanhamlett/6604662
        :param include_properties: Flag to include property values
        :return:
        """
        data = {}

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()  # type: ignore
        properties = dir(self)

        for key in columns:
            data[key] = getattr(self, key)

        if include_properties:
            for key in list(set(properties) - set(columns) - set(relationships)):
                if key.startswith('_'):
                    continue
                data[key] = getattr(self, key)

        return data

    def clone(self) -> 'BaseModel':
        return self.__class__(**self.to_dict())  # type: ignore


BaseSiteModel = declarative_base(cls=BaseModel, metadata=MetaData(schema=EQUIPMENTS_SCHEMA_NAME))  # pylint: disable=C0103

BaseApplicationModel = declarative_base(cls=BaseModel, metadata=MetaData(schema=APPLICATION_SCHEMA_NAME))  # pylint: disable=C0103

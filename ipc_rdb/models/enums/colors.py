import enum


__all__ = ['ColorsEnum']


class ColorsEnum(enum.Enum):
    UNDEFINED = 0
    GREEN = 1
    YELLOW = 2
    RED = 3

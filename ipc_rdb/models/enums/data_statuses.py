import enum
import functools
import operator
from typing import Sequence

__all__ = ['DataStatusesEnum', 'check_status', 'check_statuses', 'get_statuses', 'combine_statuses']


class DataStatusesEnumMeta(enum.EnumMeta):
    # For small int column type we have 15 available bits to use.
    # So it's 15 different statuses that we can specify.
    status_bit_length = 15

    def __new__(mcs, cls, bases, classdict):
        enum_cls = super().__new__(mcs, cls, bases, classdict)

        seen_values = set()
        for status in enum_cls.__members__.values():
            value = status.value

            if value not in range(0, mcs.status_bit_length + 1):
                raise ValueError(f'"{value}" is invalid choice for {enum_cls.__name__}. '
                                 f'Values should be in range [0, {mcs.status_bit_length}]')

            if value in seen_values:
                raise ValueError(f'{enum_cls.__name__} values should be unique')
            seen_values.add(value)

        return enum_cls


class DataStatusesEnum(enum.Enum, metaclass=DataStatusesEnumMeta):
    """
    Each value represents number of bit
    """
    ok = 0
    asset_off = 1


def check_status(value: int, status: DataStatusesEnum) -> bool:
    """
    Check if `status` is set in the `value`
    :param value: bit combination to check `status` represented by int value
    :param status: Statuses to check
    :return: bool flag that shows if status set or not
    """
    if status == DataStatusesEnum.ok:
        return not bool(value)

    return bool(value & (1 << status.value - 1))


def check_statuses(value: int, statuses: Sequence[DataStatusesEnum]) -> Sequence[bool]:
    """
    Check if `statuses` are set in the `value`
    :param value: bit combination to check `statuses` represented by int value
    :param statuses: Sequence of statuses to check
    :return: Sequence of bool flags shows if status set or not.
     Order of result sequence corresponds to the statuses order
    """
    return [check_status(value, status) for status in statuses]


def get_statuses(value: int) -> Sequence[DataStatusesEnum]:
    """
    Check for set bits in `value`

    :param value: bit combination to check for statuses represented by int value
    :return: Sequence of set statuses
    """
    return [status for status in DataStatusesEnum.__members__.values() if check_status(value, status)]


def combine_statuses(statuses: Sequence[DataStatusesEnum]) -> int:
    """
    Get integer representation of statuses
    :param statuses: Sequence of statuses to consider
    :return: Integer representation of statuses bits
    """
    bits = [1 << status.value - 1 for status in statuses if status != DataStatusesEnum.ok]
    return functools.reduce(operator.or_, bits, 0b0)

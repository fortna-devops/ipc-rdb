import enum


__all__ = ['DataAggregationEnum']

# Microsecond is Postgres' highest timestamp resolution.
# Our TS data are at microsecond resolution.
# Therefore, enum values below are microsecond-based. These
# values are used in calculations so be careful how you change them.

class DataAggregationEnum(enum.Enum):
    NO_AGGREGATION = 0
    AVG_PER_MICROSECOND = 1
    AVG_PER_MILLISECOND = 1000
    AVG_PER_SECOND = 1000000
    AVG_PER_MINUTE = 60000000
    AVG_PER_HOUR = 3600000000
    # AVG_PER_DAY = 6
    # AVG_PER_WEEK = 7

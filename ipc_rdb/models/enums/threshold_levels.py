import enum


__all__ = ['ThresholdLevelEnum']


class ThresholdLevelEnum(enum.Enum):
        YELLOW = 0
        RED = 1
        LOW_YELLOW = 2
        LOW_RED = 3

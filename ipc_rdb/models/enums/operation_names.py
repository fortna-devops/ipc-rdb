import enum

__all__ = ['OperationNamesEnum']

class OperationNamesEnum(enum.Enum):
    Non_diverting = 0x00000001
    Diverting = 0x00000002
    Night_sort = 0x00000010
    Twilight_sort = 0x00000020
    Day_sort = 0x00000030
    Preload_sort = 0x00000040
    Pretrip_active = 0x00000050

    Ready = 0x00000100
    Active = 0x00000200
    At_speed = 0x00000300
    Accelerating = 0x00000400
    Decelerating = 0x00000500

    Running = 0x00002000
    At_reference = 0x00003000

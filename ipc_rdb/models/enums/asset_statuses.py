import enum


__all__ = ['AssetStatusesEnum']


class AssetStatusesEnum(enum.Enum):
    ON = 1
    OFF = 2

    def invert(self) -> 'AssetStatusesEnum':
        if self == AssetStatusesEnum.ON:
            return AssetStatusesEnum.OFF

        return AssetStatusesEnum.ON

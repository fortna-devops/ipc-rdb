from sqlalchemy import Column, String

from .mixins import TableNameMixin, CreatedAtMixin
from .base_model import BaseSiteModel


__all__ = ['WarningsModel']


class WarningsModel(TableNameMixin, CreatedAtMixin, BaseSiteModel):

    name = Column(String, primary_key=True)
    description = Column(String, nullable=False)

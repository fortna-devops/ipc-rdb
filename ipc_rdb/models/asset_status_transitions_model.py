from typing import TYPE_CHECKING

from sqlalchemy import Column, Enum, Integer, ForeignKey, DateTime, Interval, Index
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .enums import AssetStatusesEnum
from .mixins import TableNameMixin, IdMixin, CreatedAtMixin


if TYPE_CHECKING:
    from . import AssetsModel


__all__ = ['AssetStatusTransitionsModel']


class AssetStatusTransitionsModel(TableNameMixin, IdMixin, CreatedAtMixin, BaseSiteModel):
    asset_id = Column(Integer, ForeignKey('assets.id'), nullable=False)
    status = Column(Enum(AssetStatusesEnum))
    timestamp = Column(DateTime, nullable=False)
    interval = Column(Interval, nullable=False, server_default='0')

    asset = relationship('AssetsModel', back_populates='status_transitions')

    @declared_attr
    def __table_args__(cls):  # pylint: disable=E0213
        return (
            Index(
                f'{cls.__tablename__}_asset_id_status_timestamp_desc_index',
                cls.asset_id, cls.status, cls.timestamp.desc()
            ),
        )

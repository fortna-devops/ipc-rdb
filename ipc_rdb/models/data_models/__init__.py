from typing import Union, Dict, Type

from sqlalchemy.sql.functions import func, _FunctionGenerator as FunctionGenerator

from .base_data_mixin import BaseDataMixin
from .float_data_model import RawFloatDataModel, CalculatedFloatDataModel
from .int_data_model import RawIntDataModel, CalculatedIntDataModel
from .string_data_model import RawStringDataModel, CalculatedStringDataModel
from .timestamp_data_model import RawTimestampDataModel, CalculatedTimestampDataModel

# Types definition
DataModelsType = Union[
    RawFloatDataModel,
    RawIntDataModel,
    RawStringDataModel,
    RawTimestampDataModel,
    CalculatedFloatDataModel,
    CalculatedIntDataModel,
    CalculatedStringDataModel,
    CalculatedTimestampDataModel
]


# Models definition
RAW_DATA_MODELS = (
    RawFloatDataModel,
    RawIntDataModel,
    RawStringDataModel,
    RawTimestampDataModel
)

CALCULATED_DATA_MODELS = (
    CalculatedFloatDataModel,
    CalculatedIntDataModel,
    CalculatedStringDataModel,
    CalculatedTimestampDataModel
)


# Tables definition
RAW_FLOAT_DATA_TABLE = RawFloatDataModel.__table__.name
RAW_INT_DATA_TABLE = RawIntDataModel.__table__.name
RAW_STRING_DATA_TABLE = RawStringDataModel.__table__.name
RAW_TIMESTAMP_DATA_TABLE = RawTimestampDataModel.__table__.name

CALCULATED_FLOAT_DATA_TABLE = CalculatedFloatDataModel.__table__.name
CALCULATED_INT_DATA_TABLE = CalculatedIntDataModel.__table__.name
CALCULATED_STRING_DATA_TABLE = CalculatedStringDataModel.__table__.name
CALCULATED_TIMESTAMP_DATA_TABLE = CalculatedTimestampDataModel.__table__.name

RAW_DATA_TABLES = (
    RAW_FLOAT_DATA_TABLE,
    RAW_INT_DATA_TABLE,
    RAW_STRING_DATA_TABLE,
    RAW_TIMESTAMP_DATA_TABLE
)

CALCULATED_DATA_TABLES = (
    CALCULATED_FLOAT_DATA_TABLE,
    CALCULATED_INT_DATA_TABLE,
    CALCULATED_STRING_DATA_TABLE,
    CALCULATED_TIMESTAMP_DATA_TABLE
)


# Mappings definition
DEFAULT_AGG_MAP: Dict[str, FunctionGenerator] = {
    RAW_FLOAT_DATA_TABLE: func.avg,
    RAW_INT_DATA_TABLE: func.avg,
    RAW_STRING_DATA_TABLE: func.array_agg,
    RAW_TIMESTAMP_DATA_TABLE: func.array_agg,
    CALCULATED_FLOAT_DATA_TABLE: func.avg,
    CALCULATED_INT_DATA_TABLE: func.avg,
    CALCULATED_STRING_DATA_TABLE: func.array_agg,
    CALCULATED_TIMESTAMP_DATA_TABLE: func.array_agg,
}

TABLE_NAME_MAP: Dict[str, Type[DataModelsType]] = {
    RAW_FLOAT_DATA_TABLE: RawFloatDataModel,
    RAW_INT_DATA_TABLE: RawIntDataModel,
    RAW_STRING_DATA_TABLE: RawStringDataModel,
    RAW_TIMESTAMP_DATA_TABLE: RawTimestampDataModel,
    CALCULATED_FLOAT_DATA_TABLE: CalculatedFloatDataModel,
    CALCULATED_INT_DATA_TABLE: CalculatedIntDataModel,
    CALCULATED_STRING_DATA_TABLE: CalculatedStringDataModel,
    CALCULATED_TIMESTAMP_DATA_TABLE: CalculatedTimestampDataModel,
}

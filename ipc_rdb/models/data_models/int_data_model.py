from sqlalchemy import Column, Integer

from ..base_model import BaseSiteModel
from .base_data_mixin import BaseDataMixin
from .data_statuses_mixin import DataStatusesMixin


__all__ = ['RawIntDataModel', 'CalculatedIntDataModel']


class BaseIntData(BaseDataMixin):
    value = Column(Integer, nullable=False)


class RawIntDataModel(BaseIntData, DataStatusesMixin, BaseSiteModel):
    pass


class CalculatedIntDataModel(BaseIntData, BaseSiteModel):
    pass

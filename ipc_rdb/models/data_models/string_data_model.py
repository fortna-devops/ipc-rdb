from sqlalchemy import Column, String

from ..base_model import BaseSiteModel
from .base_data_mixin import BaseDataMixin
from .data_statuses_mixin import DataStatusesMixin


__all__ = ['RawStringDataModel', 'CalculatedStringDataModel']


class BaseStringData(BaseDataMixin):
    value = Column(String, nullable=False)


class RawStringDataModel(BaseStringData, DataStatusesMixin, BaseSiteModel):
    pass


class CalculatedStringDataModel(BaseStringData, BaseSiteModel):
    pass

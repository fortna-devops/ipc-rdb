from typing import TYPE_CHECKING, ClassVar

from sqlalchemy import Column, Integer, ForeignKey, DateTime, Index, BigInteger
from sqlalchemy.sql import func
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from ..mixins import TableNameMixin, BigIdMixin, CreatedAtMixin


if TYPE_CHECKING:
    from .. import MetricsModel


__all__ = ['BaseDataMixin']


class BaseDataMixin(TableNameMixin, BigIdMixin, CreatedAtMixin):

    @declared_attr
    def metric_id(cls):  # pylint: disable=E0213
        return Column(Integer, ForeignKey('metrics.id', ondelete='RESTRICT'), nullable=False)

    @declared_attr
    def metric(cls):  # pylint: disable=E0213
        return relationship('MetricsModel')

    timestamp = Column(DateTime, nullable=False)

    value: ClassVar[Column]

    @declared_attr
    def operational_code_id(cls):  # pylint: disable=E0213
        return Column(BigInteger, ForeignKey('operational_codes.id',
                                              onupdate='CASCADE',
                                              ondelete='RESTRICT'),
                      nullable=False)

    @declared_attr
    def operational_codes(cls):  # pylint: disable=E0213
        return relationship('OperationalCodesModel')


    @declared_attr
    def __table_args__(cls):  # pylint: disable=E0213
        return (
            Index(
                f'{cls.__tablename__}_metric_id_timestamp_minute_index',
                cls.metric_id, func.date_trunc('minute', cls.timestamp)
            ),
            Index(
                f'{cls.__tablename__}_metric_id_timestamp_hour_index',
                cls.metric_id, func.date_trunc('hour', cls.timestamp)
            )
        )

from sqlalchemy import Column, DateTime

from ..base_model import BaseSiteModel
from .base_data_mixin import BaseDataMixin
from .data_statuses_mixin import DataStatusesMixin


__all__ = ['RawTimestampDataModel', 'CalculatedTimestampDataModel']


class BaseTimestampData(BaseDataMixin):
    value = Column(DateTime, nullable=False)


class RawTimestampDataModel(BaseTimestampData, DataStatusesMixin, BaseSiteModel):
    pass


class CalculatedTimestampDataModel(BaseTimestampData, BaseSiteModel):
    pass

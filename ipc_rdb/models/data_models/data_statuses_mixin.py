from typing import ClassVar, TYPE_CHECKING

from sqlalchemy import Column, Integer, Index, func, ForeignKey
from sqlalchemy.ext.declarative import declared_attr

if TYPE_CHECKING:
    from .. import OperationalCodesModel


__all__ = ['DataStatusesMixin']


class DataStatusesMixin:
    """
    Contains information relevant to the `operational_code` field in the data tables.
    """
    @declared_attr
    def operational_code_id(cls):  # pylint: disable=E0213
        return Column(Integer, ForeignKey('operational_codes.id', ondelete='RESTRICT'), nullable=False)

    __tablename__ = ClassVar[str]
    metric_id = ClassVar[Column]
    timestamp = ClassVar[Column]

    @declared_attr
    def __table_args__(cls):  # pylint: disable=E0213
        return (
            Index(
                f'{cls.__tablename__}_metric_id_timestamp_minute_operational_code_index',
                cls.metric_id, func.date_trunc('minute', cls.timestamp), cls.operational_code
            ),
            Index(
                f'{cls.__tablename__}_metric_id_timestamp_hour_operational_code_index',
                cls.metric_id, func.date_trunc('hour', cls.timestamp), cls.operational_code
            )
        )

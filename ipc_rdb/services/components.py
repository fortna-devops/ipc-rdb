from datetime import datetime
from typing import Dict, List, Optional, Sequence, Union

from sqlalchemy import func, extract
from sqlalchemy.orm import contains_eager

from .base import Base
from .mixins import OrderByCreated

from ..models import ComponentsModel, DataSourcesModel, MetricsModel, ThresholdsModel, \
    AlarmsModel, ColorsEnum, AlarmConfigsModel
from ..utils.ranges import tsrange


__all__ = ['Components']


class Components(OrderByCreated, Base):
    model = ComponentsModel

    def get_details(self, component_ids: Sequence[int]) -> List[ComponentsModel]:
        return self.query.options(
            contains_eager(
                self.model.data_sources,
                DataSourcesModel.metrics,
                MetricsModel.thresholds,
                ThresholdsModel.level
            ),
            contains_eager(
                self.model.type
            ))\
            .outerjoin(self.model.type)\
            .outerjoin(self.model.data_sources)\
            .outerjoin(DataSourcesModel.metrics)\
            .outerjoin(MetricsModel.thresholds)\
            .join(ThresholdsModel.level)\
            .filter(
                self.model.to_display.is_(True),
                MetricsModel.to_display.is_(True),
                self.model.id.in_(component_ids)
            )\
            .order_by(MetricsModel.name)\
            .all()

    def calculate_health(self,
                         start: datetime,
                         end: datetime,
                         scale_factor: float = 1.,
                         component_ids: Optional[Sequence[int]] = None) -> List[Dict[str, Union[float, int]]]:
        """
        Calculate health % per component id.

        component health % = (1 - (Σ (d * s) / (4320 * n))) * 100

            - Summation of all alarms per component over [`start`, `end`] time interval
            - d = duration of alarm (in minutes)
            - s = severity of alarm (YELLOW = 2, RED = 3)
            - n = scale factor constant increasing/decreasing impact of each alarm on overall component health rating
                  (n ∝ health rating)

        health = 100%  --->  'perfect' health
        health <= 0%  --->  'catastrophic' health
        """

        alarm_time_intersection = tsrange(AlarmsModel.started_at, AlarmsModel.ended_at, '[]') *\
                                  tsrange(start, end, '[]')
        alarm_num_minutes = extract('epoch', func.upper(alarm_time_intersection) -\
                                    func.lower(alarm_time_intersection)) / 60
        alarm_num_dmg_points = alarm_num_minutes *\
                               (func.array_position(func.enum_range(AlarmsModel.color), AlarmsModel.color) - 1)
        num_dmg_points = func.sum(alarm_num_dmg_points)

        num_minutes = (end - start).total_seconds() / 60
        max_num_dmg_points = scale_factor * num_minutes * ColorsEnum.RED.value

        query = self._session\
            .query(
                self.model.id,
                ((1 - num_dmg_points / max_num_dmg_points) * 100).label('health'),
            )\
            .join(self.model.data_sources)\
            .join(DataSourcesModel.metrics)\
            .join(MetricsModel.alarms)\
            .join(AlarmConfigsModel)\
            .filter(
                self.model.to_display.is_(True),
                MetricsModel.to_display.is_(True),
                AlarmConfigsModel.to_display.is_(True),
                AlarmsModel.started_at != AlarmsModel.ended_at,  # exclude errors and warnings alarm types.
                tsrange(
                    start, end, '[]'
                ).overlaps(
                    tsrange(AlarmsModel.started_at, AlarmsModel.ended_at, '[]')
                )
            )\
            .group_by(self.model.id)

        if component_ids:
            query = query.filter(self.model.id.in_(component_ids))

        return self._query_rows_to_dicts(query)

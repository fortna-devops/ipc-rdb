from typing import List, Optional

from .base import Base

from ..models import AnalyticsModel


__all__ = ['Analytics']


class Analytics(Base):

    model = AnalyticsModel

    def get_configs(self, active_status: Optional[bool] = None) -> List[AnalyticsModel]:

        query = self._session.query(self.model)

        if active_status is not None:
            query = query.filter(AnalyticsModel.active == active_status)

        return query.all()
        
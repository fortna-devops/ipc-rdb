from .base import Base
from ..models import ErrorsModel


__all__ = ['Errors']


class Errors(Base):

    model = ErrorsModel

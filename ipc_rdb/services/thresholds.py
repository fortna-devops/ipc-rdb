from typing import List, Optional
from .base import Base

from ..models import ThresholdsModel


__all__ = ['Thresholds']


class Thresholds(Base):
    model = ThresholdsModel


    def get_thresholds(self,
                       analytic_name: Optional[str] = None,
                       active_status: Optional[bool] = None) -> List[ThresholdsModel]:

        query = self._session.query(self.model)

        if analytic_name is not None:
            query = query.filter(ThresholdsModel.analytic == analytic_name)

        if active_status is not None:
            query = query.filter(ThresholdsModel.active == active_status)

        query = query.order_by(ThresholdsModel.metric_id.asc(),
                               ThresholdsModel.id.asc(),
                               ThresholdsModel.level_name.asc())

        return query.all()

import enum
from abc import ABC
from typing import Tuple, Dict, Any, Union, ClassVar

from sqlalchemy import func, Column
from sqlalchemy.sql import elements

from .base import Base


__all__ = ['BaseDataSource', 'DataKind']


class DataKind(enum.Enum):
    measured = 1
    predicted = 2


class BaseDataSource(Base, ABC):  # pylint: disable=W0223

    model: Any
    data_attrs: ClassVar[Tuple[Column, ...]]
    data_kind: ClassVar[DataKind]

    @property
    @classmethod
    def valid_data_ranges(cls) -> Dict[Column, Tuple[int, int]]:
        return {}

    @classmethod
    def get_data_attrs_dict(cls) -> Dict[str, elements.Label]:
        return {attr.name: func.avg(attr).label(attr.name) for attr in cls.data_attrs}

    @classmethod
    def is_data_in_range(cls, attr: Column, value: Union[int, float]) -> bool:
        # pylint: disable=E1136
        low, high = cls.valid_data_ranges[attr]  # type: ignore
        return low <= value <= high

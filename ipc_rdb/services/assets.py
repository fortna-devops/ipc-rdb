from typing import List, Sequence

from sqlalchemy.orm import contains_eager

from .base import Base
from .mixins import OrderByCreated

from ..models import AssetsModel, ComponentsModel, DataSourcesModel, MetricsModel

__all__ = ['Assets']


class Assets(OrderByCreated, Base):
    model = AssetsModel

    def get_details(self, asset_ids: Sequence[int]) -> List[AssetsModel]:
        return self.query.options(
            contains_eager(
                self.model.components,
                ComponentsModel.data_sources,
                DataSourcesModel.metrics
            ))\
            .outerjoin(self.model.components)\
            .outerjoin(ComponentsModel.data_sources)\
            .outerjoin(DataSourcesModel.metrics)\
            .filter(
                self.model.to_display.is_(True),
                ComponentsModel.to_display.is_(True),
                MetricsModel.to_display.is_(True),
                self.model.id.in_(asset_ids),
            )\
            .all()

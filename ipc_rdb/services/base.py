from abc import ABC, abstractmethod
from typing import Any, List, Dict, Sequence

from sqlalchemy import orm
from sqlalchemy.sql import elements


class Base(ABC):

    def __init__(self, session: orm.Session):
        self._session = session

    @property
    @classmethod
    @abstractmethod
    def model(cls) -> Any:
        pass

    @property
    def query(self) -> orm.Query:
        return self._session.query(self.model)

    def get_all(self) -> List[Any]:
        return self.query.all()

    def exists(self, *criterion: elements.BinaryExpression) -> bool:
        return self._session.query(self.query.filter(*criterion).exists()).scalar()

    def insert(self, instance: Any):
        self._session.add(instance)
        self._session.commit()

    def _insert_all(self, instances: Sequence[Any]):
        self._session.bulk_save_objects(instances)
        self._session.commit()

    def insert_all(self, instances: Sequence[Any], batch_size: int = 100):
        if not batch_size:
            self._insert_all(instances)
            return

        for i in range(0, len(instances), batch_size):
            self._insert_all(instances[i:i + batch_size])

    def update(self, *criterion: elements.BinaryExpression, values: Dict[str, Any]):
        self.query.filter(*criterion).update(values)
        self._session.commit()

    def delete(self, *criterion: elements.BinaryExpression):
        self.query.filter(*criterion).delete()
        self._session.commit()

    @staticmethod
    def _query_rows_to_dicts(query: orm.Query) -> List[Dict[str, Any]]:
        result = []
        for row in query:
            result.append({desc['name']: value for desc, value in zip(query.column_descriptions, row)})
        return result

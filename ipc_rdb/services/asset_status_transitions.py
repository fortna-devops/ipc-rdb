import itertools
import operator
from collections import defaultdict
from datetime import datetime, timedelta
from typing import List, Dict, Sequence, Tuple, Optional, Iterable

from sqlalchemy import func

from .base import Base
from ..models import AssetStatusTransitionsModel, AssetStatusesEnum


__all__ = ['AssetStatusTransitions']


class AssetStatusTransitions(Base):
    """
    Interface for asset status transitions

    ATTENTION!
    When we've got a new transition we calculate time elapsed between previous transition and new one.
    We store this time as an interval field in the NEW transition row.
    Example:
        Asset was ON at 05:00:00 then it become OFF at 07:38:00
        That means that asset was ON for 2h 38m
        We put this value to OFF status transition row
    To calculate total ON time we need to sum all intervals with status OFF
    """
    model = AssetStatusTransitionsModel
    status = AssetStatusesEnum

    def get_latest(self, asset_ids: Iterable[int]) -> Dict[int, AssetStatusTransitionsModel]:
        """
        Get map of latest statuses
        :param asset_ids: Iterable of asset ids to be considered
        :return: Dict with asset id as a key and latest transition for that asset as a value
        """
        latest_transitions = self._session.query(self.model)\
            .filter(self.model.asset_id.in_(asset_ids))\
            .order_by(self.model.asset_id, self.model.timestamp.desc())\
            .distinct(self.model.asset_id).all()

        return {transition.asset_id: transition for transition in latest_transitions}

    def get_total_runtime(self, asset_ids: Iterable[int]) -> Dict[int, timedelta]:
        """
        Calculate total runtime for asset
        :param asset_ids: Iterable of asset ids to be considered
        :return: Dict with asset id as a key and runtime as a value
        """
        query = self._session.query(
            self.model.asset_id,
            func.sum(self.model.interval)
            )\
            .filter(
                self.model.asset_id.in_(asset_ids),
                self.model.status == self.status.OFF
            )\
            .group_by(self.model.asset_id)
        return dict(query)

    def group_by_asset_id(self, asset_ids: Sequence[int], start_datetime: datetime, end_datetime: datetime
                          ) -> Dict[int, List[AssetStatusTransitionsModel]]:
        """
        Get map of status transitions
        :param asset_ids: Iterable of asset ids to be considered
        :param start_datetime: Lower limit of data timestamp
        :param end_datetime: Upper limit of data timestamp
        :return: Dict of asset id as a key and list of status transitions as a value
        """
        result: Dict[int, List[AssetStatusTransitionsModel]] = defaultdict(list)
        transitions = self.query\
            .filter(
                self.model.asset_id.in_(asset_ids),
                AssetStatusTransitionsModel.timestamp.between(start_datetime, end_datetime)
            ).order_by(self.model.asset_id, AssetStatusTransitionsModel.timestamp.desc())\
            .all()

        for transition in transitions:
            result[transition.asset_id].append(transition)

        return result

    @staticmethod
    def _transition_order_key(item: AssetStatusTransitionsModel) -> Tuple[int, datetime]:
        """
        Build a key to sort transition instances
        :param item: `AssetStatusTransitionsModel` to be sorted
        :return: Sorting key
        """
        return item.asset_id, item.timestamp

    @staticmethod
    def _is_to_insert(existing: Optional[AssetStatusTransitionsModel], new: AssetStatusTransitionsModel) -> bool:
        """
        We insert transition in next cases:
        1. If there is no previously stored transition
        2. If arrived status is newer and it's not the same as we already have

        :param existing: existing instance of `AssetStatusTransitionsModel`
        :param new: instance of `AssetStatusTransitionsModel` that we want to insert
        :return: True - if instance should be inserted
        """
        if existing is not None and existing.asset_id != new.asset_id:
            raise ValueError('Only transitions for the same asset can be compared.')

        return existing is None or (new.timestamp > existing.timestamp and new.status != existing.status)

    @staticmethod
    def _prepare_to_insert(existing: Optional[AssetStatusTransitionsModel], new: AssetStatusTransitionsModel):
        """
        Set the time interval that has passed since the previous transition.
        If there is no previous transition then 0 will be set by default
        :param existing: existing instance of `AssetStatusTransitionsModel`
        :param new: instance of `AssetStatusTransitionsModel` that we want to insert
        """
        if existing is not None:
            new.interval = new.timestamp - existing.timestamp

    def insert(self, instance: AssetStatusTransitionsModel):
        """
        Insert new asset status transition in RDB
        :param instance: `AssetStatusTransitionsModel` to insert
        """
        latest = self.get_latest(asset_ids=[instance.asset_id]).get(instance.asset_id)

        if self._is_to_insert(existing=latest, new=instance):
            self._prepare_to_insert(existing=latest, new=instance)
            super().insert(instance=instance)

    def insert_all(self, instances: Sequence[AssetStatusTransitionsModel], batch_size: int = 100):
        """
        Insert new asset status transitions in RDB
        :param instances: Sequence of `AssetStatusTransitionsModel` to insert
        :param batch_size: number of instances to be inserted at once
        """
        asset_ids = {transition.asset_id for transition in instances}
        latest_transitions = self.get_latest(asset_ids=asset_ids)

        transitions: List[AssetStatusTransitionsModel] = []
        ordered_transitions = sorted(instances, key=self._transition_order_key)
        for asset_id, transitions_grp in itertools.groupby(ordered_transitions, operator.attrgetter('asset_id')):

            latest = latest_transitions.get(asset_id)
            for instance in transitions_grp:
                if not self._is_to_insert(existing=latest, new=instance):
                    continue

                self._prepare_to_insert(existing=latest, new=instance)

                latest = instance
                transitions.append(instance)

        super().insert_all(instances=transitions, batch_size=batch_size)

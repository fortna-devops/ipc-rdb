from typing import Tuple, List, Sequence, Optional

from sqlalchemy.orm import contains_eager

from .base import Base
from .mixins import OrderByCreated

from ..models import DataSourcesModel, MetricsModel, ThresholdsModel


__all__ = ['DataSources']


class DataSources(OrderByCreated, Base):

    model = DataSourcesModel

    def get_ids(self, type_names: Tuple[str, ...] = ()) -> List[int]:

        from ..models import DataSourceTypesModel  # pylint: disable=C0415
        query = self._session.query(self.model.id)\
            .join(DataSourceTypesModel)

        if type_names:
            query = query.filter(DataSourceTypesModel.name.in_(type_names))

        query = query.order_by(DataSourcesModel.id)

        return [data_source['id'] for data_source in self._query_rows_to_dicts(query)]

    def get_with_metrics_and_thresholds(self,
                                        ids: Optional[Sequence[int]] = None,
                                        metric_to_analyze: Optional[bool] = None,
                                        analytic_names: Optional[Sequence[str]] = None) -> List[DataSourcesModel]:
        '''
        Return metric_id's and threshold values for data source ids in ids.
        '''
        if (ids is None) or (not ids):
            return []

        query = self.query\
            .options(
                contains_eager(self.model.metrics, MetricsModel.thresholds, ThresholdsModel.level)
            )\
            .join(self.model.metrics)\
            .join(MetricsModel.thresholds)\
            .join(ThresholdsModel.level)

        query = query.filter(self.model.id.in_(ids))

        if metric_to_analyze is not None:
            query = query.filter(MetricsModel.to_analyze == metric_to_analyze)

        if analytic_names:
            query = query.filter(ThresholdsModel.analytic.in_(analytic_names))

        return query.all()

    def get_data_source_info_for_metrics(self, metric_ids: List[int] = None) -> List[int]:
        """
        Return rows of data_sources table associated with the supplied metric_ids.
        """
        query = self.query\
            .options(contains_eager(self.model.metrics))\
            .join(self.model.metrics)


        if metric_ids:
            query = query.filter(self.model.metrics.id.in_(metric_ids))

        return query.distinct().all()

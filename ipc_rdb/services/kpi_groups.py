from .base import Base
from ..models import KpiGroupsModel


__all__ = ['KpiGroups']


class KpiGroups(Base):
    model = KpiGroupsModel

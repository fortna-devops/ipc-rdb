from typing import List, Sequence, Dict, Any, Optional, Union, Callable
from datetime import datetime

from sqlalchemy import Column, Date
from sqlalchemy.orm import contains_eager
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import Label

from .base import Base
from ..models import AlarmsModel, ColorsEnum, AlarmConfigsModel
from ..models import MetricsModel, DataSourcesModel, ComponentsModel

from ..utils.ranges import tsrange, daterange


__all__ = ['Alarms']


class Alarms(Base):

    model = AlarmsModel

    @staticmethod
    def _trim_dates(alarms: List[AlarmsModel], start: datetime, end: datetime) -> List[AlarmsModel]:

        for alarm in alarms:

            if alarm.started_at < start:
                alarm.started_at = start

            # if alarm does not have ended_at value, it is null, then return it unchanged
            if alarm.ended_at and alarm.ended_at > end:
                alarm.ended_at = end

        return alarms

    def _insert(self, alarm: AlarmsModel, alarm_at_start: bool=False):
        """
        Inserts new a alarm. 
        An alarm has null ended_at if it was still alarming at the end of the analytic
        execution. A metric can have only one null ended_at row in the Alarms table.
        A null ended_at alarm is terminated (has a value assigned to ended_at) if
        the current alarm starts at the beginning of analytic execution.

        alarm - current alarm to be inserted into table
        alarm_at_start - the 'alarm' being inserted started at the beginning of  
                         the analytic execution (indicating that it was a continuation
                         of a prior alarm)
        """

        existing_alarms = self.query.filter(
            alarm.analytic_name == self.model.analytic_name,
            alarm.color == self.model.color,
            alarm.metric_id == self.model.metric_id,
            self.model.ended_at == None,
        ).all()

        if len(existing_alarms) == 0:
            # normal scenario
            pass
        elif len(existing_alarms) == 1:
            if alarm_at_start:
                #This is the normal, expected scenario.
                if alarm.ended_at:
                    # Only need to update ended_at column of existing alarm,
                    # and not insert current alarm into table 
                    # (existing_alarms[0].ended_at should be null, if not something is wrong)
                    existing_alarms[0].ended_at = alarm.ended_at
                    return
                else:
                    # Alarm was still continuing at end of execution time, 
                    # so leave ended_at empty, and do not insert current alarm.
                    return
            else:
                # Something is wrong if we get here. If there is an existing alarm
                # with null ended_at, alarm_at_start should be True.
                # In anycase, we try to recover and must ensure that the existing alarm 
                # with null ended_at must have a value.
                existing_alarms[0].ended_at = alarm.started_at  # Note, we're not connecting existing_alarm to alarm
        else:
            # Something is wrong here with the alarms in the table.
            # There should only be one alarm with null ended_at per metric id.
            # (Maybe the analytic is executed multiple times over the same time range?)
            pass

        self._session.add(alarm)

    def get_color_by_component_id(self,
                                  start: datetime,
                                  end: datetime,
                                  agg_func_callback: Callable[[Column], Column],
                                  component_ids: Optional[Sequence[int]] = None) -> List[Dict[str, Any]]:

        component_id_column = DataSourcesModel.component_id.label('component_id')
        query = self._session.query(
            agg_func_callback(self.model.color).label('color'),
            component_id_column
        )\
            .filter(AlarmConfigsModel.to_display)\
            .join(self.model.metric)\
            .join(MetricsModel.data_source)\
            .group_by(component_id_column)

        query = query.filter(
            tsrange(
                self.model.started_at, self.model.ended_at, '[]'
            ).overlaps(
                tsrange(start, end, '[]')
            )
        )

        if component_ids:
            query = query.filter(component_id_column.in_(component_ids))

        return self._query_rows_to_dicts(query)

    def filter(self,
               start: datetime,
               end: datetime,
               asset_ids: Optional[Sequence[int]] = None,
               component_ids: Optional[Sequence[int]] = None,
               data_source_ids: Optional[Sequence[int]] = None,
               metric_ids: Optional[Sequence[int]] = None,
               to_display: Optional[bool] = True,
               colors: Optional[Sequence[ColorsEnum]] = None,
               analytic_names: Optional[Sequence[str]] = None,
               offset: Optional[int] = None,
               limit: Optional[int] = None,
               count: bool = False) -> Union[int, List[AlarmsModel]]:

        query = self.query\
            .options(
                contains_eager(
                    self.model.metric,
                    MetricsModel.data_source,
                    DataSourcesModel.component,
                    ComponentsModel.asset
                ),
                contains_eager(
                    self.model.metric,
                    MetricsModel.data_source,
                    DataSourcesModel.type
                ),
                contains_eager(
                    self.model.metric,
                    MetricsModel.data_source,
                    DataSourcesModel.component,
                    ComponentsModel.type
                )
            )\
            .join(AlarmConfigsModel)\
            .join(self.model.metric)\
            .join(MetricsModel.data_source)\
            .join(DataSourcesModel.component)\
            .join(ComponentsModel.asset)\
            .join(DataSourcesModel.type)\
            .join(ComponentsModel.type)

        query = query.filter(
            tsrange(
                self.model.started_at, self.model.ended_at, '[]'
            ).overlaps(
                tsrange(start, end, '[]')
            )
        )

        if asset_ids:
            query = query.filter(ComponentsModel.asset_id.in_(asset_ids))

        if component_ids:
            query = query.filter(DataSourcesModel.component_id.in_(component_ids))

        if data_source_ids:
            query = query.filter(MetricsModel.data_source_id.in_(data_source_ids))

        if metric_ids:
            query = query.filter(self.model.metric_id.in_(metric_ids))

        if to_display is not None:
            query = query.filter(AlarmConfigsModel.to_display.is_(to_display))

        if colors:
            query = query.filter(self.model.color.in_(colors))

        if analytic_names:
            query = query.filter(self.model.analytic_name.in_(analytic_names))

        query = query.order_by(self.model.metric_id, self.model.analytic_name, self.model.started_at)

        if offset:
            query = query.offset(offset)

        if limit:
            query = query.limit(limit)

        if count:
            return query.count()

        return self._trim_dates(query.all(), start, end)

    def count(self,
              group_by: Sequence[Label],
              start: datetime,
              end: datetime,
              asset_ids: Optional[Sequence[int]] = None,
              component_ids: Optional[Sequence[int]] = None,
              data_source_ids: Optional[Sequence[int]] = None,
              metric_ids: Optional[Sequence[int]] = None,
              to_display: Optional[bool] = True) -> List[Dict[str, Any]]:

        query = self._session.query(
            *group_by,
            func.count(self.model.id).label('num_alarms')
        )\
            .join(AlarmConfigsModel)\
            .join(self.model.metric)\
            .join(MetricsModel.data_source)\
            .join(DataSourcesModel.component)\
            .join(ComponentsModel.asset)\
            .group_by(*group_by)

        query = query.filter(
            tsrange(
                self.model.started_at, self.model.ended_at, '[]'
            ).overlaps(
                tsrange(start, end, '[]')
            )
        )

        if asset_ids:
            query = query.filter(ComponentsModel.asset_id.in_(asset_ids))

        if component_ids:
            query = query.filter(DataSourcesModel.component_id.in_(component_ids))

        if data_source_ids:
            query = query.filter(MetricsModel.data_source_id.in_(data_source_ids))

        if metric_ids:
            query = query.filter(self.model.metric_id.in_(metric_ids))

        if to_display is not None:
            query = query.filter(AlarmConfigsModel.to_display.is_(to_display))

        return self._query_rows_to_dicts(query)

    def count_by_date(self,
                      group_by: Sequence[Label],
                      start: datetime,
                      end: datetime,
                      timezone: str = '0',
                      asset_ids: Optional[Sequence[int]] = None,
                      component_ids: Optional[Sequence[int]] = None,
                      data_source_ids: Optional[Sequence[int]] = None,
                      metric_ids: Optional[Sequence[int]] = None,
                      to_display: Optional[bool] = True) -> List[Dict[str, Any]]:

        series = self._session.query(
            func.generate_series(start, end, '1 day').label('timestamp')
        ).subquery('time_series')

        timezone_op = 'at time zone'

        on_clause = daterange(
            AlarmsModel.started_at.op(timezone_op)(timezone).cast(Date),
            AlarmsModel.ended_at.op(timezone_op)(timezone).cast(Date),
            '[]'
        ).contains(series.c.timestamp.op(timezone_op)(timezone).cast(Date))

        query = self._session.query(
            series.c.timestamp.op(timezone_op)(timezone).cast(Date).label('date'),
            *group_by,
            func.count(self.model.id).label('num_alarms')
        )\
            .outerjoin(self.model, on_clause)\
            .join(AlarmConfigsModel)\
            .join(self.model.metric)\
            .join(MetricsModel.data_source)\
            .join(DataSourcesModel.component)\
            .join(ComponentsModel.asset)\
            .group_by(series.c.timestamp, *group_by)\
            .order_by(series.c.timestamp)

        query = query.filter(
            tsrange(
                self.model.started_at, self.model.ended_at, '[]'
            ).overlaps(
                tsrange(start, end, '[]')
            )
        )

        if asset_ids:
            query = query.filter(ComponentsModel.asset_id.in_(asset_ids))

        if component_ids:
            query = query.filter(DataSourcesModel.component_id.in_(component_ids))

        if data_source_ids:
            query = query.filter(MetricsModel.data_source_id.in_(data_source_ids))

        if metric_ids:
            query = query.filter(self.model.metric_id.in_(metric_ids))

        if to_display is not None:
            query = query.filter(AlarmConfigsModel.to_display.is_(to_display))

        return self._query_rows_to_dicts(query)

    def insert(self, instance: AlarmsModel, alarm_at_start: bool=False):
        self._insert(instance, alarm_at_start)
        self._session.commit()

    def insert_all(self, instances: Sequence[AlarmsModel],
                    batch_size: int = 0):

        for alarm in instances:
            self.insert(alarm)

    def insert_all_with_flags(self, instances: Sequence[AlarmsModel],
                    alarm_at_starts: Sequence[bool],
                    batch_size: int = 0):

        for alarm, alarm_at_start in zip(instances, alarm_at_starts):
            self.insert(alarm, alarm_at_start)

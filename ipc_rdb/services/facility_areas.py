from typing import List

from sqlalchemy.orm import contains_eager

from .base import Base
from .mixins import OrderByCreated
from ..models import FacilityAreasModel, AssetsModel, ComponentsModel


__all__ = ['FacilityAreas']


class FacilityAreas(OrderByCreated, Base):

    model = FacilityAreasModel

    def get_with_assets_and_components(self) -> List[FacilityAreasModel]:

        return self.query\
            .options(
                contains_eager(self.model.assets, AssetsModel.components, ComponentsModel.type),
                contains_eager(self.model.assets, AssetsModel.type)
            )\
            .filter(
                self.model.to_display.is_(True),
                AssetsModel.to_display.is_(True),
                ComponentsModel.to_display.is_(True)
            )\
            .outerjoin(self.model.assets)\
            .outerjoin(AssetsModel.components)\
            .join(AssetsModel.type)\
            .join(ComponentsModel.type)\
            .order_by(self.model.name.asc(), AssetsModel.name.asc(), ComponentsModel.name.asc())\
            .all()

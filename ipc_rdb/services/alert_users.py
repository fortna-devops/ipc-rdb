from typing import List

from sqlalchemy.orm import contains_eager

from .base import Base
from .mixins import OrderByCreated
from ..models import AlertUsersModel


__all__ = ['AlertUsers']


class AlertUsers(OrderByCreated, Base):

    model = AlertUsersModel

    def get_all(self) -> List[AlertUsersModel]:

        return self.query.options(
            contains_eager(self.model.alert_roles),
            contains_eager(self.model.alert_black_list)
        )\
            .filter(self.model.active)\
            .join(self.model.alert_roles)\
            .outerjoin(self.model.alert_black_list)\
            .all()

from .base import Base
from .mixins import OrderByCreated

from ..models import DataSourceTypesModel


__all__ = ['DataSourceTypes']


class DataSourceTypes(OrderByCreated, Base):
    model = DataSourceTypesModel

from .base import Base
from .mixins import OrderByCreated
from ..models import AlertRolesModel, AlertRolesEnum


__all__ = ['AlertRoles']


class AlertRoles(OrderByCreated, Base):
    model = AlertRolesModel

    role = AlertRolesEnum

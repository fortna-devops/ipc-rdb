from .base import Base
from .mixins import OrderByCreated

from ..models import AssetTypesModel


__all__ = ['AssetTypes']


class AssetTypes(OrderByCreated, Base):
    model = AssetTypesModel

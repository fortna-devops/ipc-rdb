from datetime import datetime
from typing import Sequence, List, Optional, Union, Dict, Any

from sqlalchemy import func
from sqlalchemy.orm import contains_eager

from .base import Base
from .mixins import OrderByCreated

from ..models import MetricsModel, DataSourcesModel, DataSourceTypesModel, ComponentsModel, ComponentTypesModel,\
    AssetsModel, AlarmsModel, AlarmConfigsModel, ColorsEnum
from ..utils.ranges import tsrange


__all__ = ['Metrics']


class Metrics(OrderByCreated, Base):
    model = MetricsModel

    def get_for_display(self) -> List[MetricsModel]:
        return self.query.filter(self.model.to_display.is_(True)).order_by(self.model.name).all()

    def get_for_reports(self) -> List[Dict[str, Any]]:

        query = self._session.query(
            self.model.display_name
        )\
            .filter(self.model.to_display.is_(True))\
            .group_by(self.model.display_name)\
            .order_by(self.model.display_name)

        return self._query_rows_to_dicts(query)

    def get_alarming(self,
                     start: datetime,
                     end: datetime,
                     display_names: Sequence[str] = (),
                     to_display: Optional[bool] = True,
                     analytic_name_names: Sequence[str] = (),
                     alarm_colors: Sequence[ColorsEnum] = (),
                     alarm_configs_to_display: Optional[bool] = True,
                     offset: Optional[int] = None,
                     limit: Optional[int] = None,
                     count: bool = False) -> Union[int, List[Dict[str, Any]]]:

        group_by_columns = (
            AssetsModel.name.label('asset_name'),
            ComponentsModel.asset_id,
            ComponentsModel.name.label('component_name'),
            ComponentTypesModel.name.label('component_type_name'),
            DataSourcesModel.component_id,
            DataSourceTypesModel.name.label('data_source_type_name'),
            self.model.data_source_id,
            self.model.id,
            self.model.display_name,
            self.model.units
        )

        query = self._session.query(
            *group_by_columns,
            func.count(AlarmsModel.id).label('num_alarms')
        )\
            .join(self.model.data_source)\
            .join(DataSourcesModel.type)\
            .join(DataSourcesModel.component)\
            .join(ComponentsModel.type)\
            .join(ComponentsModel.asset)\
            .join(self.model.alarms)\
            .group_by(*group_by_columns)

        query = query.filter(
            tsrange(
                AlarmsModel.started_at, AlarmsModel.ended_at, '[]'
            ).overlaps(
                tsrange(start, end, '[]')
            )
        )

        if display_names:
            query = query.filter(self.model.display_name.in_(display_names))

        if to_display is not None:
            query = query.filter(self.model.to_display.is_(to_display))

        if analytic_name_names:
            query = query.filter(AlarmsModel.analytic_name.in_(analytic_name_names))

        if alarm_colors:
            query = query.filter(AlarmsModel.color.in_(alarm_colors))

        if alarm_configs_to_display is not None:
            query = query.filter(AlarmConfigsModel.to_display.is_(alarm_configs_to_display))

        query = query.order_by(
            ComponentsModel.asset_id,
            DataSourcesModel.component_id,
            self.model.display_name
        )

        if offset:
            query = query.offset(offset)

        if limit:
            query = query.limit(limit)

        if count:
            return query.count()

        return self._query_rows_to_dicts(query)

    def get_with_asset(self,
                       component_types: List[str] = None,
                       metric_names: List[str] = None,
                       component_to_display: Optional[bool] = None,
                       ) -> List[MetricsModel]:
        """
        Get metrics with all levels up to asset (metric, data_source, component, and asset).
        """
        query = self.query\
            .options(contains_eager(self.model.data_source, DataSourcesModel.component, ComponentsModel.asset))\
            .join(self.model.data_source)\
            .join(DataSourcesModel.component)\
            .join(ComponentsModel.asset)

        if component_types:
            query = query.filter(ComponentTypesModel.name.in_(component_types))

        if metric_names:
            query = query.filter(MetricsModel.name.in_(metric_names))

        if component_to_display is not None:
            query = query.filter(ComponentsModel.to_display.is_(component_to_display))

        return query.all()


    def get_data_source_ids(self, metric_ids: List[int] = None) -> List[int]:
        """
        Get unique data_source_ids for the metric_ids provided.
        """
        query = self._session.query(self.model.data_source_id)

        query = query.filter(self.model.id.in_(metric_ids))

        return query.distinct().all()

    def get_data_source_and_metric_ids(self, metric_ids: List[int] = None) -> List[MetricsModel]:
        """
        Return rows of data_sources table associated with the supplied metric_ids.
        """
        # The below retrieves all craps attached by 'relationship's.
        # query = self.query\
        #     .options(contains_eager(self.model.data_source))\
        #     .join(self.model.data_source)

        query = self.query\
            .options(contains_eager(self.model.data_source))\
            .join(self.model.data_source)

        if metric_ids:
            query = query.filter(self.model.id.in_(metric_ids))

        return query.distinct().all()

from .base import Base
from .mixins import OrderByCreated
from ..models import GatewaysModel


__all__ = ['Gateways']


class Gateways(OrderByCreated, Base):
    model = GatewaysModel

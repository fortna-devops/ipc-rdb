from typing import List, Dict, Any

from sqlalchemy import orm

__all__ = ['query_rows_to_dicts']


def query_rows_to_dicts(query: orm.Query) -> List[Dict[str, Any]]:
    result = []
    for row in query:
        result.append({desc['name']: value for desc, value in zip(query.column_descriptions, row)})
    return result

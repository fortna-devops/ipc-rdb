__all__ = ['DataValidationError']


class DataValidationError(ValueError):
    pass

import logging
from typing import TYPE_CHECKING, Type, Tuple, Optional, Dict, List, Any, Sequence, ClassVar, Callable
from datetime import datetime

from sqlalchemy import func, orm, or_
from sqlalchemy.sql import operators, elements, expression

#from build.lib.ipc_rdb.models.enums import data_aggregation

from ...models import AssetsModel, ComponentsModel, DataSourcesModel, MetricsModel
from ...models.enums import DataStatusesEnum, DataAggregationEnum, combine_statuses
from ...models.data_models import RAW_DATA_MODELS, DataModelsType
from ...utils.query import query_rows_to_dicts

from .data_points import DataPoints
from .query_unit import MetricQueryUnits
from .error import DataValidationError
from .group_level import GroupLevel, GroupModels


if TYPE_CHECKING:  # pragma: no cover
    import pandas as pd


__all__ = ['DataProxy']


logger = logging.getLogger()


class DataProxy:
    """
    Provides an interface to grab data

    `get_grouped` - method is the generic interface to query data
    `get_grouped_by_metric_id`, `get_transposed_by_metric_id` - wrappers that feeds required parameters to
    `get_grouped`

    Data can be queried form any `data` table. Take a look to the models.new.data_models module
    for the list of available tables.

    During data aggregation next rules will be applied by default:
        For `float` data `func.avg` will be applied.
        For `int` data `func.avg` will be applied.
        For `string` data `func.array_agg` will be applied.

    This behavior can be overridden by passing combination of metric instance and aggregation function. Take a look
    to `get_grouped` description for more info.
    """

    # Sampling configuration
    microsecond_sample_rate: ClassVar[str] = 'microsecond'
    hour_sample_rate: ClassVar[str] = 'hour'

    minute_sample_rate: ClassVar[str] = 'minute'
    second_sample_rate: ClassVar[str] = 'second'

    sample_rate_options: ClassVar[Tuple[str, ...]] = (
        hour_sample_rate, minute_sample_rate, second_sample_rate, microsecond_sample_rate
    )

    _data_aggregation_map = {
        DataAggregationEnum.NO_AGGREGATION: microsecond_sample_rate,
        DataAggregationEnum.AVG_PER_MICROSECOND: microsecond_sample_rate,
        DataAggregationEnum.AVG_PER_MILLISECOND: microsecond_sample_rate,
        DataAggregationEnum.AVG_PER_SECOND: second_sample_rate,
        DataAggregationEnum.AVG_PER_MINUTE: minute_sample_rate,
        DataAggregationEnum.AVG_PER_HOUR: hour_sample_rate
    }

    # Ordering configuration
    asc_ordering: ClassVar[str] = 'asc'
    desc_ordering: ClassVar[str] = 'desc'

    ordering_options: ClassVar[Tuple[str, ...]] = (
        asc_ordering, desc_ordering
    )

    # Grouping configuration
    asset_group_level: ClassVar[GroupLevel] = GroupLevel(AssetsModel, 'asset_id')
    component_group_level: ClassVar[GroupLevel] = GroupLevel(ComponentsModel, 'component_id')
    data_source_group_level: ClassVar[GroupLevel] = GroupLevel(DataSourcesModel, 'data_source_id')
    metrics_group_level: ClassVar[GroupLevel] = GroupLevel(MetricsModel)

    group_level_options: ClassVar[Tuple[GroupLevel, ...]] = (
        asset_group_level, component_group_level, data_source_group_level, metrics_group_level
    )

    # Data kind configuration
    raw_data_kind: ClassVar[str] = 'raw'
    calculated_data_kind: ClassVar[str] = 'calculated'

    data_kind_options: ClassVar[Tuple[str, ...]] = (
        raw_data_kind, calculated_data_kind
    )

    def __init__(self, session: orm.Session):
        self._session = session

    @classmethod
    def _validate(cls, start_datetime: datetime, end_datetime: datetime,
                  sample_rate: str, group_level: Optional[GroupLevel]):
        """
        Validate input parameters.
        """
        if end_datetime < start_datetime:
            raise DataValidationError(f'end_date: {end_datetime} less than start_date: {start_datetime}')

        if sample_rate not in cls.sample_rate_options:
            raise DataValidationError(f'"{sample_rate}" is not an option for sample_rate')

        if group_level and group_level not in cls.group_level_options:
            raise DataValidationError(f'"{group_level}" is not an option for group_level')

        if group_level and sample_rate == cls.microsecond_sample_rate:
            raise DataValidationError(f'"{cls.microsecond_sample_rate}" sample rate can\'t be used with grouping')


    @classmethod
    def _get_order_data_key(cls, timestamp_order: str) -> Callable[[Dict[str, Any]], Tuple[Any, ...]]:
        """
        Provide ordering key based on order priorities.
        """

        def _order_data_key(item: Dict[str, Any]) -> Tuple[Any, ...]:
            if timestamp_order == cls.asc_ordering:
                timestamp_key = item['timestamp']

            elif timestamp_order == cls.desc_ordering:
                timestamp_key = datetime.max - item['timestamp']

            else:
                raise DataValidationError(f'"{timestamp_order}" is not an option for timestamp_order')

            return (
                cls.data_kind_options.index(item.get('kind', cls.raw_data_kind)),
                item.get('asset_id'),
                item.get('component_id'),
                item.get('data_source_id'),
                item['metric_id'],
                timestamp_key
            )

        return _order_data_key

    @classmethod
    def _order_data(cls, data: Sequence[Dict[str, Any]], *, timestamp_order: str = desc_ordering
                    ) -> Sequence[Dict[str, Any]]:
        return sorted(data, key=cls._get_order_data_key(timestamp_order))

    def _get_metrics(self, metric_ids: Sequence[int]) -> List[MetricsModel]:
        return self._session.query(MetricsModel).filter(MetricsModel.id.in_(metric_ids)).all()

    def _get_metric_query_units(self, metric_ids: Sequence[int], data_kinds: Tuple[str, ...]) -> MetricQueryUnits:
        metric_query_units = MetricQueryUnits()
        for metric in self._get_metrics(metric_ids):
            metric_query_units.add_metric(metric=metric, data_kinds=data_kinds)
        return metric_query_units

    @staticmethod
    def _get_flags_filter(data_model_cls: Type[DataModelsType], statuses: Tuple[DataStatusesEnum, ...]
                          ) -> Optional[elements.ClauseElement]:
        if not statuses or not issubclass(data_model_cls, RAW_DATA_MODELS):
            return None

        parts: List[elements.ClauseElement] = []

        combined = combine_statuses(statuses)
        if DataStatusesEnum.ok in statuses:
            parts.append(data_model_cls.flags == 0)  # type: ignore

        if combined:
            parts.append(data_model_cls.flags.op('&')(combined) > 0)  # type: ignore

        return or_(*parts)

    def get_transposed_by_metric_id(self, start_datetime: datetime, end_datetime: datetime,
                                    metric_ids: List[int],
                                    sample_rate: str = minute_sample_rate) -> 'pd.DataFrame':
        """
        Interface to get data grouped and transposed by metric id
        Should be used in machine learning.

        :param start_datetime: Lower limit of data timestamp
        :param end_datetime: Upper limit of data timestamp
        :param metric_ids: List of metric ids to query
        :param sample_rate: Data sampling rate
        :return: pandas DataFrame
        metric_id                     1         2        3
        timestamp
        2020-08-28 09:36:00  219.997004  0.979949  3.89001
        2020-08-28 09:37:00   87.448029  2.486292  2.25473
        2020-08-28 09:38:00   29.296452       NaN  3.80423
        """
        from ..transpose import transpose  # pylint: disable=C0415
        metric_query_units = self._get_metric_query_units(metric_ids=metric_ids, data_kinds=(self.raw_data_kind,))
        data = self.get_data(start_datetime=start_datetime,
                             end_datetime=end_datetime,
                             metric_query_units=metric_query_units,
                             sample_rate=sample_rate,
                             group_level=self.metrics_group_level,
                             include_kind=False)
        return transpose(data)

    def get_grouped_by_metric_id(self, start_datetime: datetime, end_datetime: datetime,
                                 metric_ids: List[int],
                                 data_aggregation: DataAggregationEnum=DataAggregationEnum.AVG_PER_SECOND,  #str = minute_sample_rate,
                                 opereration_code_id: int = 1,
                                 timestamp_order: str = asc_ordering
                                ) -> Sequence[Dict[str, Any]]:
        """
        Interface to get data grouped by metric id
        Should be used in `analytics-calculator` lambda

        :param start_datetime: Lower limit of data timestamp
        :param end_datetime: Upper limit of data timestamp
        :param metric_ids: List of metric ids to query
        :param sample_rate: Data sampling rate
        :param timestamp_order: Order of data timestamp sorting
        :return: Sequence of data points grouped by metric id.
        [
            {
                'metric_id': 1,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
                'value': 87.4480293818882
            },
            {
                'metric_id': 1,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 38),
                'value': 29.2964515686035
            },
            {
                'metric_id': 2,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 36),
                'value': 0.979949474334717
            },
            {
                'metric_id': 2,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
                'value': 2.48629232247671
            },
            {
                'metric_id': 3,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
                'value': 2.25473
            },
            {
                'metric_id': 3,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 38),
                'value': 3.80423
            }
        ]
        """

        sample_rate = self._data_aggregation_map.get(data_aggregation, self.minute_sample_rate)

        group_level: Optional[GroupLevel] = None
        if sample_rate != self.microsecond_sample_rate:
            group_level = self.metrics_group_level

        metric_query_units = self._get_metric_query_units(metric_ids=metric_ids, data_kinds=(self.raw_data_kind,))
        data = self.get_data(start_datetime=start_datetime,
                             end_datetime=end_datetime,
                             metric_query_units=metric_query_units,
                             sample_rate=sample_rate,
                             opereration_codes_id=opereration_code_id,
                             group_level=group_level,
                             statuses=(DataStatusesEnum.ok,), include_kind=False)

        return self._order_data(data, timestamp_order=timestamp_order)

    def get_grouped_with_kind(self, start_datetime: datetime, end_datetime: datetime,
                              metric_ids: List[int],
                              sample_rate: str = minute_sample_rate,
                              opereration_code_id: int = 1,
                              timestamp_order: str = desc_ordering,
                              group_level: GroupLevel = metrics_group_level,
                              data_kinds: Tuple[str, ...] = (raw_data_kind, calculated_data_kind),
                              include_kind: bool = True,
                             ) -> Sequence[Dict[str, Any]]:
        """
        Interface to get data from "raw" and/or "calculated" tables
        Should be used in data API

        :param start_datetime: Lower limit of data timestamp
        :param end_datetime: Upper limit of data timestamp
        :param metric_ids: List of metric ids to query
        :param sample_rate: Data sampling rate
        :param timestamp_order: Order of data timestamp sorting
        :param group_level: Level to group data by. By default will be grouped by `metrics_group_level`
        :param data_kinds: Tuple of "kinds" of data to be queried
        :param include_kind: If set to `True` then `kind` key will appear in each row
        :return: Dict with data kind as a key and Sequence of data points grouped by metric id as a value
        [
            {
                'kind': 'raw',
                'metric_id': 1,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
                'value': 87.4480293818882
            },
            {
                'kind': 'raw',
                'metric_id': 2,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 36),
                'value': 0.979949474334717
            },
            {
                'kind': 'raw',
                'metric_id': 3,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
                'value': 2.25473
            }
            {
                'kind': 'calculated',
                'metric_id': 1,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 38),
                'value': 29.2964515686035
            },
            {
                'kind': 'calculated',
                'metric_id': 2,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
                'value': 2.48629232247671
            },
            {
                'kind': 'calculated',
                'metric_id': 3,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 38),
                'value': 3.80423
            }
        ]
        """
        metric_query_units = self._get_metric_query_units(metric_ids=metric_ids, data_kinds=data_kinds)
        data = self.get_data(start_datetime=start_datetime,
                             end_datetime=end_datetime,
                             metric_query_units=metric_query_units,
                             sample_rate=sample_rate,
                             opereration_codes_id=opereration_code_id,
                             group_level=group_level,
                             include_kind=include_kind)

        return self._order_data(data, timestamp_order=timestamp_order)

    # pylint: disable=R0912,R0914
    def get_data(self, start_datetime: datetime,
                 end_datetime: datetime,
                 metric_query_units: MetricQueryUnits,
                 sample_rate: str = hour_sample_rate,
                 opereration_codes_id: int = 1,
                 group_level: Optional[GroupLevel] = metrics_group_level,
                 statuses: Tuple[DataStatusesEnum, ...] = (),  # pylint: disable=W0613
                 include_kind: bool = False) -> Sequence[Dict[str, Any]]:
        """
        Generic interface for data query

        :param start_datetime: Lower limit of data timestamp
        :param end_datetime: Upper limit of data timestamp
        :param metric_query_units: `MetricQueryUnits` container
        :param sample_rate: Data sampling rate
        :param group_level: Level to group data by. By default will be grouped by `metrics_group_level`
        :param statuses: Sequence of `DataStatusesEnum` to consider
        :param include_kind: If set to `True` then `kind` key will appear in each row
        :return: Sequence of data points grouped by specified the `group_level`

        [
            {
                'asset_id': 1,
                'component_id': 1,
                'data_source_id': 1,
                'metric_id': 1,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
                'value': 87.4480293818882
            },
            {
                'asset_id': 1,
                'component_id': 1,
                'data_source_id': 1,
                'metric_id': 1,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 38),
                'value': 29.2964515686035
            },
            {
                'asset_id': 1,
                'component_id': 1,
                'data_source_id': 1,
                'metric_id': 2,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 36),
                'value': 0.979949474334717
            },
            {
                'asset_id': 1,
                'component_id': 1,
                'data_source_id': 1,
                'metric_id': 2,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
                'value': 2.48629232247671
            },
            {
                'asset_id': 1,
                'component_id': 1,
                'data_source_id': 1,
                'metric_id': 3,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
                'value': 2.25473
            },
            {
                'asset_id': 1,
                'component_id': 1,
                'data_source_id': 1,
                'metric_id': 3,
                'timestamp': datetime.datetime(2020, 8, 28, 9, 38),
                'value': 3.80423
            }
        ]
        """
        self._validate(start_datetime=start_datetime, end_datetime=end_datetime, sample_rate=sample_rate,
                       group_level=group_level)

        data: List[Dict[str, Any]] = []
        for data_model_cls, query_parameters, data_kind, metric_ids in metric_query_units:

            group_attrs: List[operators.ColumnOperators] = []
            query_attrs: List[elements.ColumnElement] = [data_model_cls.metric_id]
            filter_attrs: List[elements.ClauseElement] = [data_model_cls.metric_id.in_(metric_ids)]
            filter_attrs.append(data_model_cls.operational_code_id.__eq__(opereration_codes_id))
            join_attrs: List[Tuple[GroupModels, elements.ColumnClause]] = []


            for label, data_agg_func in query_parameters:
                # When sample_rate equals self.microsecond_sample_rate, data_agg_func should be None but
                # for now, we add the 2nd condition bc we dont want to mess around with query_parameters
                if data_agg_func is not None and (sample_rate != self.microsecond_sample_rate):
                    value_attr = data_agg_func(data_model_cls.value).label(label)  # type: ignore
                else:
                    value_attr = data_model_cls.value  # type: ignore
                query_attrs.append(value_attr)

            _timestamp = func.date_trunc(sample_rate, data_model_cls.timestamp)
            query_attrs.append(_timestamp.label('timestamp'))
            filter_attrs.append(_timestamp.between(start_datetime, end_datetime))

            if sample_rate != self.microsecond_sample_rate:
                group_attrs.extend([_timestamp, data_model_cls.metric_id])

            if include_kind:
                query_attrs.append(expression.bindparam('kind', data_kind))

            if group_level:
                if group_level != self.metrics_group_level:
                    join_attrs.append((MetricsModel, MetricsModel.id == data_model_cls.metric_id))

                levels = self.group_level_options[self.group_level_options.index(group_level):]
                saved_level: Optional[GroupLevel] = None
                for level in reversed(levels):
                    if saved_level is not None and level.attr_name is not None:
                        join_attrs.append((level.model, level.model.id == getattr(saved_level.model, level.attr_name)))
                    saved_level = level

                    if level == self.metrics_group_level:
                        # We don't need data from the metrics table itself
                        continue

                    attr = level.model.id.label(level.attr_name)
                    query_attrs.append(attr)
                    group_attrs.append(attr)

            query = self._session\
                .query(*query_attrs)\
                .filter(*filter_attrs)\
                .group_by(*reversed(group_attrs))

            if join_attrs:
                for join in join_attrs:
                    query = query.outerjoin(join)

            data.extend(query_rows_to_dicts(query))

        return data

    def _insert(self, data_model_cls: Type[DataModelsType], data: Sequence[Dict[str, Any]]):
        self._session.bulk_insert_mappings(data_model_cls, data)
        self._session.commit()

    def insert(self, data_points: DataPoints, batch_size: int = 1000):
        """
        Insert data to the corresponding data tables
        :param data_points: `DataPoints` container
        :param batch_size: Maximum umber of data points will be inserted at once
        :return:
        """
        for data_model_cls, data in data_points:
            for i in range(0, len(data), batch_size):
                self._insert(data_model_cls=data_model_cls, data=data[i:i + batch_size])

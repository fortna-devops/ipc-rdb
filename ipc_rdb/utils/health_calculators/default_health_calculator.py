from datetime import datetime
from typing import Dict, Optional, Sequence

from ipc_rdb.services import Components
from ipc_rdb.utils.health_calculators.base_health_calculator import BaseHealthCalculator


__all__ = ['DefaultHealthCalculator']


class DefaultHealthCalculator(BaseHealthCalculator):
    """
    Calculates health.
    """

    _scale_factor = 1.
    _low_health_floor = 30.

    # pylint: disable=W0221
    def calculate(self,
                  alarm_start: datetime,
                  alarm_end: datetime,
                  component_ids: Optional[Sequence[int]] = None) -> Dict[int, float]:
        """
        Calculates health rating per component id.
        """

        component_healths = Components(self._session).calculate_health(
            start=alarm_start,
            end=alarm_end,
            scale_factor=self._scale_factor,
            component_ids=component_ids
        )

        return {
            component['id']: max(component['health'], self._low_health_floor)  # type: ignore
            for component in component_healths
        }

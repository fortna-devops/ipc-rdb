from typing import Sequence
from datetime import datetime

from sqlalchemy.sql import func

from .base_color_calculator import BaseColorCalculator
from ... import Alarms
from .colors import Colors


__all__ = ['DefaultColorCalculator']


class DefaultColorCalculator(BaseColorCalculator):
    """
    Default color calculator.

    Calculates colors based on max alarm severity.
    """

    def calculate(self, alarm_start: datetime, alarm_end: datetime, component_ids: Sequence[int] = ()) -> Colors:
        """
        Calculates colors.
        """

        components = Alarms(self._session).get_color_by_component_id(
            start=alarm_start,
            end=alarm_end,
            agg_func_callback=func.max,
            component_ids=component_ids
        )

        colors = Colors()

        for component in components:
            colors.set_color(component['component_id'], component['color'])

        return colors

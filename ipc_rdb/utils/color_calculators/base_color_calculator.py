from abc import ABC, abstractmethod
from typing import Sequence
from datetime import datetime

from sqlalchemy.orm import Session

from .colors import Colors


__all__ = ['BaseColorCalculator']


class BaseColorCalculator(ABC):
    """
    Base color calculator.
    """

    def __init__(self, session: Session):
        self._session = session

    @abstractmethod
    def calculate(self, alarm_start: datetime, alarm_end: datetime, component_ids: Sequence[int] = ()) -> Colors:
        """
        Calculates colors for components with alarms.
        """

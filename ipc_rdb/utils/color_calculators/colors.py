from typing import Dict

from ...models import ColorsEnum


__all__ = ['Colors']


class Colors:

    def __init__(self):
        self._colors: Dict[int, ColorsEnum] = {}

    def __repr__(self) -> str:
        return str(self._colors)

    def get_color(self, component_id: int) -> ColorsEnum:
        return self._colors.get(component_id, ColorsEnum.GREEN)

    def set_color(self, component_id: int, color: ColorsEnum):
        self._colors[component_id] = color

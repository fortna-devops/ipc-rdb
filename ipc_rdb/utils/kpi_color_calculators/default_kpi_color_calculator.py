from typing import Sequence
from datetime import datetime

from sqlalchemy.sql import func

from .base_kpi_color_calculator import BaseKpiColorCalculator
from .kpi_colors import KpiColors

from ... import Kpis


__all__ = ['DefaultKpiColorCalculator']


class DefaultKpiColorCalculator(BaseKpiColorCalculator):
    """
    Default kpi color calculator.

    Calculates colors based on max alarm color.
    """

    def calculate(self, alarm_start: datetime, alarm_end: datetime, asset_ids: Sequence[int] = ()) -> KpiColors:
        """
        Calculates colors.
        """

        kpis = Kpis(self._session).get_color_by_id(
            agg_func_callback=func.max,
            alarm_start=alarm_start,
            alarm_end=alarm_end,
            asset_ids=asset_ids
        )

        kpi_colors = KpiColors()
        for kpi in kpis:
            kpi_colors.set_color(kpi['id'], kpi['color'])

        return kpi_colors

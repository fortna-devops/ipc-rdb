from functools import partial

from sqlalchemy import func
from sqlalchemy.dialects.postgresql import TSRANGE, DATERANGE


__all__ = ['tsrange', 'daterange']


tsrange = partial(func.tsrange, type_=TSRANGE)  # pylint: disable=C0103
daterange = partial(func.daterange, type_=DATERANGE)  # pylint: disable=C0103

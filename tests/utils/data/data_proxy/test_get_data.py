from datetime import datetime, timedelta

import pytest
from sqlalchemy import func

from ipc_rdb.utils.data import DataProxy, MetricQueryUnits
from ipc_rdb.models.data_models import RAW_FLOAT_DATA_TABLE, RAW_INT_DATA_TABLE, RAW_STRING_DATA_TABLE,\
    CALCULATED_FLOAT_DATA_TABLE, CALCULATED_INT_DATA_TABLE, CALCULATED_STRING_DATA_TABLE

from tests.utils.data import get_metrics


@pytest.mark.parametrize('metric_ids, data_tables, agg_functions, data_kind, group_level, call_count', (
    ([], [], [], DataProxy.raw_data_kind, DataProxy.metrics_group_level,  0),

    ([1], [RAW_FLOAT_DATA_TABLE], [func.avg], DataProxy.raw_data_kind, DataProxy.metrics_group_level, 1),
    ([1], [RAW_FLOAT_DATA_TABLE], [None], DataProxy.raw_data_kind, None, 1),
    ([2], [RAW_INT_DATA_TABLE], [func.max], DataProxy.raw_data_kind, DataProxy.metrics_group_level, 1),
    ([3], [RAW_STRING_DATA_TABLE], [func.array_agg], DataProxy.raw_data_kind, DataProxy.metrics_group_level, 1),
    ([1, 2, 3], [RAW_FLOAT_DATA_TABLE, RAW_INT_DATA_TABLE, RAW_STRING_DATA_TABLE], [func.avg, func.avg, func.array_agg], DataProxy.raw_data_kind, DataProxy.metrics_group_level, 3),
    ([1, 2], [RAW_FLOAT_DATA_TABLE, RAW_FLOAT_DATA_TABLE], [func.avg, func.max], DataProxy.raw_data_kind, DataProxy.metrics_group_level, 2),

    ([1], [CALCULATED_FLOAT_DATA_TABLE], [func.avg], DataProxy.calculated_data_kind, DataProxy.metrics_group_level, 1),
    ([1], [CALCULATED_FLOAT_DATA_TABLE], [None], DataProxy.calculated_data_kind, None, 1),
    ([2], [CALCULATED_INT_DATA_TABLE], [func.max], DataProxy.calculated_data_kind, DataProxy.metrics_group_level, 1),
    ([3], [CALCULATED_STRING_DATA_TABLE], [func.array_agg], DataProxy.calculated_data_kind, DataProxy.metrics_group_level, 1),
    ([1, 2, 3], [CALCULATED_FLOAT_DATA_TABLE, CALCULATED_INT_DATA_TABLE, CALCULATED_STRING_DATA_TABLE], [func.avg, func.avg, func.array_agg], DataProxy.calculated_data_kind, DataProxy.metrics_group_level, 3),
    ([1, 2], [CALCULATED_FLOAT_DATA_TABLE, CALCULATED_FLOAT_DATA_TABLE], [func.avg, func.max], DataProxy.calculated_data_kind, DataProxy.metrics_group_level, 2),
))
def test_get_data(db_session, metric_ids, data_tables, agg_functions, data_kind, group_level, call_count):
    end_datetime = datetime.utcnow()
    start_datetime = end_datetime - timedelta(minutes=1)
    sample_rate = DataProxy.hour_sample_rate

    query_units = MetricQueryUnits()
    for metric, agg_func in zip(get_metrics(metric_ids, data_tables, data_kind), agg_functions):
        query_units.add_metric(metric, (data_kind,), {'value': agg_func})

    DataProxy(db_session).get_data(
        start_datetime=start_datetime, end_datetime=end_datetime, metric_query_units=query_units,
        group_level=group_level, sample_rate=sample_rate
    )

    assert db_session.query.call_count == call_count

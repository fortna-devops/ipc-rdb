from datetime import timedelta

import pytest

from ipc_rdb.utils.data import DataProxy

from tests.utils.data import DATA_TIMESTAMP, get_data_dicts


@pytest.mark.parametrize('input_data, expected_data, order', (
    ([], [], DataProxy.asc_ordering),
    ([
        {
            'timestamp': DATA_TIMESTAMP,
            'metric_id': 1,
            'value': 0.1
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=3),
            'metric_id': 1,
            'value': 0.4
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=1),
            'metric_id': 1,
            'value': 0.2
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=2),
            'metric_id': 1,
            'value': 0.3
        }
    ],
    [
        {
            'timestamp': DATA_TIMESTAMP,
            'metric_id': 1,
            'value': 0.1
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=1),
            'metric_id': 1,
            'value': 0.2
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=2),
            'metric_id': 1,
            'value': 0.3
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=3),
            'metric_id': 1,
            'value': 0.4
        },
    ],
    DataProxy.asc_ordering),
    ([
        {
            'timestamp': DATA_TIMESTAMP,
            'metric_id': 1,
            'value': 0.1
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=3),
            'metric_id': 1,
            'value': 0.4
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=1),
            'metric_id': 1,
            'value': 0.2
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=2),
            'metric_id': 1,
            'value': 0.3
        }
    ],
    [
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=3),
            'metric_id': 1,
            'value': 0.4
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=2),
            'metric_id': 1,
            'value': 0.3
        },
        {
            'timestamp': DATA_TIMESTAMP + timedelta(minutes=1),
            'metric_id': 1,
            'value': 0.2
        },
        {
            'timestamp': DATA_TIMESTAMP,
            'metric_id': 1,
            'value': 0.1
        },
    ],
    DataProxy.desc_ordering)
))
def test_order_data(input_data, expected_data, order):
    assert DataProxy._order_data(input_data, timestamp_order=order) == expected_data

import pytest

from ipc_rdb.models import MetricsModel
from ipc_rdb.utils.data import DataProxy, DataPoints

from ipc_rdb.models.data_models import RAW_FLOAT_DATA_TABLE, RAW_INT_DATA_TABLE, RAW_STRING_DATA_TABLE,\
    CALCULATED_FLOAT_DATA_TABLE, CALCULATED_INT_DATA_TABLE, CALCULATED_STRING_DATA_TABLE

from tests.utils.data import DATA_TIMESTAMP, get_insert_data


insert_data_cases = (
    ([], [], [], [], [], DataProxy.raw_data_kind, 10, 0),

    ([1], [DATA_TIMESTAMP], [0.2], [0], [RAW_FLOAT_DATA_TABLE], DataProxy.raw_data_kind, 10, 1),
    ([2], [DATA_TIMESTAMP], [5], [0], [RAW_INT_DATA_TABLE], DataProxy.raw_data_kind, 10, 1),
    ([3], [DATA_TIMESTAMP], ['test'], [0], [RAW_STRING_DATA_TABLE], DataProxy.raw_data_kind, 10, 1),
    ([1, 2, 3], [DATA_TIMESTAMP, DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 5, 'test'], [0, 0, 0], [RAW_FLOAT_DATA_TABLE, RAW_INT_DATA_TABLE, RAW_STRING_DATA_TABLE], DataProxy.raw_data_kind, 10, 3),
    ([1, 2], [DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 0.3], [0, 0], [RAW_FLOAT_DATA_TABLE, RAW_FLOAT_DATA_TABLE], DataProxy.raw_data_kind, 1, 2),

    ([1], [DATA_TIMESTAMP], [0.2], [None], [CALCULATED_FLOAT_DATA_TABLE], DataProxy.calculated_data_kind, 10, 1),
    ([2], [DATA_TIMESTAMP], [5], [None], [CALCULATED_INT_DATA_TABLE], DataProxy.calculated_data_kind, 10, 1),
    ([3], [DATA_TIMESTAMP], ['test'], [None], [CALCULATED_STRING_DATA_TABLE], DataProxy.calculated_data_kind, 10, 1),
    ([1, 2, 3], [DATA_TIMESTAMP, DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 5, 'test'], [None, None, None], [CALCULATED_FLOAT_DATA_TABLE, CALCULATED_INT_DATA_TABLE, CALCULATED_STRING_DATA_TABLE], DataProxy.calculated_data_kind, 10, 3),
    ([1, 2], [DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 0.3], [None, None], [CALCULATED_FLOAT_DATA_TABLE, CALCULATED_FLOAT_DATA_TABLE], DataProxy.calculated_data_kind, 1, 2),
)


@pytest.mark.parametrize('metric_ids, timestamps, values, flags, data_tables, data_kind, batch_size, call_count', insert_data_cases)
def test_insert(db_session, metric_ids, timestamps, values, flags, data_tables, data_kind, batch_size, call_count):
    data_points = DataPoints(data_kind)

    for data_table, row in get_insert_data(metric_ids, timestamps, values, flags, data_tables):
        metric = MetricsModel(id=row.pop('metric_id'))
        setattr(metric, f'{data_kind}_data_table', data_table)
        data_points.add_data(metric=metric, **row)

    DataProxy(db_session).insert(data_points, batch_size=batch_size)
    assert db_session.commit.call_count == call_count

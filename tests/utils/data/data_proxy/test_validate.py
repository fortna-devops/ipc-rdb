from datetime import datetime, timedelta

import pytest

from ipc_rdb.models import MetricsModel
from ipc_rdb.utils.data import DataProxy, GroupLevel, DataValidationError


@pytest.mark.parametrize(
    'sample_rate, group_level, start_datetime, end_datetime',
    [
        (DataProxy.hour_sample_rate, DataProxy.metrics_group_level, datetime.utcnow() - timedelta(minutes=1), datetime.utcnow()),
        (DataProxy.minute_sample_rate, DataProxy.metrics_group_level, datetime.utcnow() - timedelta(minutes=1), datetime.utcnow()),
        (DataProxy.hour_sample_rate, DataProxy.data_source_group_level, datetime.utcnow() - timedelta(minutes=1), datetime.utcnow()),
        (DataProxy.minute_sample_rate, DataProxy.data_source_group_level, datetime.utcnow() - timedelta(minutes=1), datetime.utcnow()),
        (DataProxy.hour_sample_rate, DataProxy.component_group_level, datetime.utcnow() - timedelta(minutes=1), datetime.utcnow()),
        (DataProxy.minute_sample_rate, DataProxy.component_group_level, datetime.utcnow() - timedelta(minutes=1), datetime.utcnow()),
        (DataProxy.hour_sample_rate, DataProxy.asset_group_level, datetime.utcnow() - timedelta(minutes=1), datetime.utcnow()),
        (DataProxy.minute_sample_rate, DataProxy.asset_group_level, datetime.utcnow() - timedelta(minutes=1), datetime.utcnow()),
        pytest.param(DataProxy.unsampled_rate, DataProxy.metrics_group_level, datetime.utcnow() - timedelta(minutes=1), datetime.utcnow(), marks=pytest.mark.xfail(raises=DataValidationError, reason='sample rate can\'t be used with grouping')),
        pytest.param('wrong', DataProxy.metrics_group_level, datetime.utcnow() - timedelta(minutes=1), datetime.utcnow(), marks=pytest.mark.xfail(raises=DataValidationError, reason='is not an option for sample_rate')),
        pytest.param(DataProxy.hour_sample_rate, DataProxy.metrics_group_level, datetime.utcnow() + timedelta(minutes=1), datetime.utcnow(), marks=pytest.mark.xfail(raises=DataValidationError, reason='less than start_date')),
        pytest.param(DataProxy.hour_sample_rate, GroupLevel(MetricsModel, 'wrong'), datetime.utcnow() - timedelta(minutes=1), datetime.utcnow(), marks=pytest.mark.xfail(raises=DataValidationError, reason='is not an option for group_level'))
    ]
)
def test_validate(sample_rate, group_level, start_datetime, end_datetime):
    DataProxy._validate(start_datetime=start_datetime, end_datetime=end_datetime,
                        sample_rate=sample_rate, group_level=group_level)

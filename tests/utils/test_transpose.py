import pandas as pd
import pytest

from ipc_rdb.utils.transpose import transpose

from tests.utils.data import QUERY_TEST_DATA, get_data_dicts


@pytest.mark.parametrize('metric_ids, timestamps, values, data_tables, data_kind', QUERY_TEST_DATA)
def test_transpose(metric_ids, timestamps, values, data_tables, data_kind):
    expected_df = pd.DataFrame([values], columns=metric_ids, index=list(set(timestamps)))
    result_df = transpose(get_data_dicts(metric_ids, timestamps, values))
    assert result_df.equals(expected_df)

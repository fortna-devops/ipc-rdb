from datetime import datetime

from ipc_rdb.models.data_models import \
    RAW_FLOAT_DATA_TABLE, RAW_INT_DATA_TABLE, RAW_STRING_DATA_TABLE, \
    CALCULATED_FLOAT_DATA_TABLE, CALCULATED_INT_DATA_TABLE, CALCULATED_STRING_DATA_TABLE

from ipc_rdb.utils.data import DataProxy
from ipc_rdb.models import MetricsModel


DATA_TIMESTAMP = datetime.utcnow()


QUERY_TEST_DATA = (
    ([], [], [], [], DataProxy.raw_data_kind),

    ([1], [DATA_TIMESTAMP], [0.2], [RAW_FLOAT_DATA_TABLE], DataProxy.raw_data_kind),
    ([2], [DATA_TIMESTAMP], [5], [RAW_INT_DATA_TABLE], DataProxy.raw_data_kind),
    ([3], [DATA_TIMESTAMP], ['test'], [RAW_STRING_DATA_TABLE], DataProxy.raw_data_kind),
    ([1, 2, 3], [DATA_TIMESTAMP, DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 5, 'test'], [RAW_FLOAT_DATA_TABLE, RAW_INT_DATA_TABLE, RAW_STRING_DATA_TABLE], DataProxy.raw_data_kind),
    ([1, 2], [DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 0.3], [RAW_FLOAT_DATA_TABLE, RAW_FLOAT_DATA_TABLE], DataProxy.raw_data_kind),

    ([1], [DATA_TIMESTAMP], [0.2], [CALCULATED_FLOAT_DATA_TABLE], DataProxy.calculated_data_kind),
    ([2], [DATA_TIMESTAMP], [5], [CALCULATED_INT_DATA_TABLE], DataProxy.calculated_data_kind),
    ([3], [DATA_TIMESTAMP], ['test'], [CALCULATED_STRING_DATA_TABLE], DataProxy.calculated_data_kind),
    ([1, 2, 3], [DATA_TIMESTAMP, DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 5, 'test'], [CALCULATED_FLOAT_DATA_TABLE, CALCULATED_INT_DATA_TABLE, CALCULATED_STRING_DATA_TABLE], DataProxy.calculated_data_kind),
    ([1, 2], [DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 0.3], [CALCULATED_FLOAT_DATA_TABLE, CALCULATED_FLOAT_DATA_TABLE], DataProxy.calculated_data_kind),
)


def get_insert_data(metric_ids, timestamps, values, flags, data_tables):
    for metric_id, timestamp, value, flags_, data_table in zip(metric_ids, timestamps, values, flags, data_tables):
        point = {
            'timestamp': timestamp,
            'metric_id': metric_id,
            'value': value,
            'flags': flags_
        }
        yield data_table, point


def get_data_dicts(metric_ids, timestamps, values):
    for metric_id, timestamp, value in zip(metric_ids, timestamps, values):
        point = {
            'timestamp': timestamp,
            'metric_id': metric_id,
            'value': value
        }
        yield point


def get_metrics(metric_ids, data_tables, data_kind):
    metrics = []
    for metric_id, data_table in zip(metric_ids, data_tables):
        metric = MetricsModel(id=metric_id)
        setattr(metric, f'{data_kind}_data_table', data_table)
        metrics.append(metric)
    return metrics

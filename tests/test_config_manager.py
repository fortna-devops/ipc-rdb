from ipc_rdb import Connector


def test_connector(config_manager):
    assert isinstance(config_manager.connector, Connector)


def test_customers(cm_config, config_manager):
    assert config_manager.customers == cm_config.customers


def test_sites(cm_config, config_manager, session_execute):
    assert config_manager.sites(cm_config.customer) == cm_config.sites
    cm_config.schema_names_mock.assert_called_once_with()


def test_customer_sites(cm_config, config_manager, session_execute):
    assert config_manager.customer_sites == {cm_config.customer: cm_config.sites}
    cm_config.schema_names_mock.assert_called_once_with()

from sqlalchemy.engine import interfaces

from ipc_rdb.connector import Connector, get_env_var
from ipc_rdb.session import Session


def test_options():
    option_name = 'TEST'
    option_value = 'some_value'

    options_data = {
        option_name: option_value
    }

    value = get_env_var(option_name, options_data)
    assert option_value == value


def test_env(monkeypatch):
    option_name = 'TEST'
    option_value = 'some_value'

    options_data = {}

    monkeypatch.setenv(f'AWS_RDB_{option_name}', option_value)
    value = get_env_var(option_name, options_data)
    assert option_value == value


def test_env_lower(monkeypatch):
    option_name = 'test'
    option_value = 'some_value'

    options_data = {}

    monkeypatch.setenv(f'AWS_RDB_{option_name.upper()}', option_value)
    value = get_env_var(option_name, options_data)
    assert option_value == value


def test_priority(monkeypatch):
    option_name = 'TEST'
    option_value = 'some_value'

    options_data = {
        option_name: option_value
    }

    monkeypatch.setenv(f'AWS_RDB_{option_name}', 'some_other_value')
    value = get_env_var(option_name, options_data)
    assert option_value == value


def test_default():
    option_name = 'TEST'
    option_value = 'some_value'

    options_data = {}

    value = get_env_var(option_name, options_data, option_value)
    assert option_value == value


def test_get_engine():
    host = 'test_host'
    db = 'test_db'
    c = Connector(host=host)
    engine = c.get_engine(db)

    assert str(engine.url) == f'postgresql://{host}:5432/{db}'


def test_get_credentials(rds):
    host = 'test_host'
    username = 'test_username'
    c = Connector(host=host, username=username)
    credentials = c._get_credentials()

    assert credentials['user'] == username
    assert credentials['password']


def test_get_session(rds, session_execute):
    host = 'test_host'
    db = 'test_db'
    schema = 'test_schema'

    c = Connector(host=host)
    session = c.get_session(db, schema)
    assert isinstance(session, Session)
    session_execute.assert_called_once_with('SET search_path TO :schema', {'schema': schema})


def test_on_do_connect(rds):
    host = 'test_host'
    username = 'test_username'
    c = Connector(host=host, username=username)
    params = {}
    dialect = interfaces.Dialect()
    c._on_do_connect(dialect, None, [], params)

    assert params['user'] == username
    assert params['password']

from sqlalchemy.dialects import postgresql
from sqlalchemy.engine.strategies import MockEngineStrategy

from ipc_rdb.models.base_model import BaseSiteModel


if __name__ == '__main__':
    with open('schema.sql', 'w') as f:

        def executor(sql, *_multiparams, **_params):
            f.write(str(sql.compile(dialect=postgresql.dialect())))
            f.write(';\n')

        connection = MockEngineStrategy().create('postgresql://', executor=executor)
        BaseSiteModel.metadata.create_all(connection)

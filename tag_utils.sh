#!/usr/bin/env bash

set -e

ACTION_NAME=$1
TAG_NAME="v$(python3 setup.py -V)"


function check_tag {
    if [[ ${BITBUCKET_PR_DESTINATION_BRANCH} != "master" ]]; then
        echo "PR not in to master. Exiting."
        exit 0
    fi
    if [[ $(git tag -l ${TAG_NAME}) ]]; then
        echo "Tag '${TAG_NAME}' already exists. It seems like you forgot to increase version."
        exit 1
    fi
}

function set_tag {
    git tag ${TAG_NAME}
    git push origin --tags
}

case $1 in
    "check")
        check_tag;;
    "set")
        set_tag;;
    *);;
esac
